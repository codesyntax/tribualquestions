import json

from django.core.management.base import BaseCommand

from tribualquestions.geo.models import Country

class Command(BaseCommand):
    args = '<json file path>'
    help = 'Datu definizioaren fitxategiaren patha'

    def handle(self, *args, **options):
        if not args:
            print 'Argumentuak behar ditugu: nazioen csv-a (izena, biztanleak, zona)'
            return None
        else:
            json_path = args[0]
            Country.objects.all().delete()
            with open(json_path, 'r') as f:
                data = json.loads(f.read())
                for d in data:
                    Country.objects.create(name=d.get('name'),
                                           capital=d.get('capital'),
                                           region=d.get('region'),
                                           population=d.get('population'),
                                           area=d.get('area', 0),
                                           top_level_domain=json.dumps(d.get('topLevelDomain')),
                                           currency=json.dumps(d.get('currencies')),
                                           languages=json.dumps(d.get('languages')))
                    print d.get('name')
                                           
            
