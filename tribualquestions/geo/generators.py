import random
import json
from tribualquestions.generator import Generator
from tribualquestions.geo.models import Country

class GeoGenerator(Generator):
    def _get_title(self, obj, **kwargs):
        return kwargs.get('title').upper()

    def _get_correct_answer(self, obj, **kwargs):
        return kwargs.get('correct')

    def _get_incorrect_answers(self, obj, **kwargs):
        incorrect = kwargs.get('incorrect')
        correct = kwargs.get('correct')
        if len(incorrect) != 2 or correct in incorrect:
            return ()
        return (incorrect[0], incorrect[1])

def get_incorrect_answers_biztanle_tramoak(biztanleak):
    tramoak = ((0,250000,'< 250.000'),
               (250000,1000000,'250.000 - 1M'),
               (1000000,5000000,'1M - 5M'),
               (5000000,10000000,'5M - 10M'),
               (10000000,20000000,'10M - 20M'),
               (20000000,50000000,'20M - 50M'),
               (50000000,100000000,'50M -100M'),
               (100000000,150000000,'100M - 150M'),
               (150000000,200000000,'150M - 200M'),
               (200000000,500000000,'200M - 500M'),
               (500000000,1000000000,'500M - 1.000'),               
               (1000000000,2000000000,'> 1000000000'),             
              )
    t = 0
    for tramoa in tramoak:
        if biztanleak >= tramoa[0] and biztanleak <=tramoa[1]:
            break
        t += 1

    if t==0:
        return (tramoa, tramoak[1], tramoak[2])
    elif t==1:        
        i = random.randint(1,2)
        if i==1:
            return (tramoa, tramoak[t+1], tramoak[t+2])
        else:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
    elif t==len(tramoak)-1:        
        return (tramoa, tramoak[t-1], tramoak[t-2])            
    elif t==len(tramoak)-2:        
        i = random.randint(1,2)
        if i==1:
            return (tramoa, tramoak[t-1], tramoak[t-2])
        else:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
    else:
        i = random.randint(1,3)
        if i==1:
            return (tramoa, tramoak[t+1], tramoak[t+2])
        elif i==2:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
        else:
            return (tramoa, tramoak[t-1], tramoak[t-2])            


    return None

def _get_incorrects_from_list(correct, incorrect_qs):
    mix = incorrect_qs.order_by('?')
    return mix[:2]

def _get_incorrects_from_set(correct, incorrect_set):
    copy_set = incorrect_set.copy()
    copy_set.remove(correct)
    incorrect_list = list(copy_set)
    random.shuffle(incorrect_list)
    return incorrect_list[:2]
    
def _load_languages():
    langs = set()
    for l in Country.objects.all().values_list('languages', flat=True):
        for lang in json.loads(l):
            langs.add(lang)
    return langs

def _load_currencies():
    currencies = set()
    for c in Country.objects.all().values_list('currency', flat=True):
        for cur in json.loads(c):
            currencies.add(cur)
    return currencies

def createPopulationQuestions():
    qtype = u'zenbat biztanle dauzka'
    for country in Country.objects.all():
        title = country.name
        correct, incorrect_1, incorrect_2 = get_incorrect_answers_biztanle_tramoak(country.population)
        correct = correct[2]
        incorrect = (incorrect_1[2], incorrect_2[2])
        generator = GeoGenerator()
        generator.qtype = qtype
        generator.create(obj=None, **locals())
        generator.submit()
        generator.print_galdera()
        
def createHiriburuaQuestions():
    qtype = u'zein da hiriburua'
    responses = Country.objects.all().values_list('capital',flat=True)
    for country in Country.objects.all():
        title = country.name
        correct = country.capital
        incorrect = _get_incorrects_from_list(correct, responses.exclude(capital=correct))
        generator = GeoGenerator()
        generator.qtype = qtype
        generator.create(obj=None, **locals())
        generator.submit()
        generator.print_galdera()

def createLanguageQuestions():
    qtype = u'zein da hizkuntza ofiziala'
    languages = _load_languages()
    for country in Country.objects.all():
        lang = json.loads(country.languages)
        if len(lang) == 1:
            title = country.name
            correct = lang[0]
            incorrect = _get_incorrects_from_set(correct, languages)
            generator = GeoGenerator()
            generator.qtype = qtype
            generator.create(obj=None, **locals())
            generator.submit()
            generator.print_galdera()

def createCurrencyQuestions():
    qtype = u'zein txnapon darabilte'
    currencies = _load_currencies()
    for country in Country.objects.all():
        cur = json.loads(country.currency)
        if len(cur) == 1:
            title = country.name
            correct = cur[0]
            incorrect = _get_incorrects_from_set(correct, currencies)
            generator = GeoGenerator()
            generator.qtype = qtype
            generator.create(obj=None, **locals())
            generator.submit()
            generator.print_galdera()

def _get_minor_countries_same_region(correct, all_countries):
    minimun = correct.population * 0.3
    maximun = correct.population * 0.6
    qs = all_countries.filter(population__gt=minimun, population__lt=maximun, region=correct.region).order_by('?')
    if qs.count() >= 2:
        return (qs[0].name, qs[1].name)
    
def createBiggerQuestions():
    qtype = u'Nations'
    all_countries = Country.objects.all()
    for country in all_countries:
        title = u'Zeinek dauka biztanle gehiago?'
        correct = country.name
        incorrect = _get_minor_countries_same_region(country, all_countries)
        if incorrect is not None:
            generator = GeoGenerator()
            generator.create(obj=None, **locals())
            generator.submit()
            generator.print_galdera()
        
