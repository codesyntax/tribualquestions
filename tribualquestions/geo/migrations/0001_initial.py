# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('capital', models.CharField(max_length=255)),
                ('region', models.CharField(max_length=255)),
                ('population', models.IntegerField()),
                ('area', models.IntegerField()),
                ('top_level_domain', models.CharField(max_length=10)),
                ('currency', models.CharField(max_length=255)),
                ('languages', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
