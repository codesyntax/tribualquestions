# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0002_auto_20150515_1417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='area',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
    ]
