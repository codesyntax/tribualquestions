from django.db import models

class Country(models.Model):
    name = models.CharField(max_length=255)
    capital = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    population = models.IntegerField()
    area = models.IntegerField(default=0,blank=True,null=True)
    top_level_domain = models.CharField(max_length=10)
    currency = models.CharField(max_length=255)
    languages = models.CharField(max_length=255)
                                        
