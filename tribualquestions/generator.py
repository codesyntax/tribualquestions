import requests
import json

from django.conf import settings

API_BASE_URL = getattr(settings, 'TRIBUAL_API_BASE_URL', 'https://tribual.codesyntax.com/api/v1/question')
TRIBUAL_TOKEN = getattr(settings, 'TRIBUAL_API_TOKEN', '')

class Generator(object):
    qtype = ""
    title = ""
    desc = ""
    photo_url = ""
    correct_answer = ""
    incorrect_answer_one = ""
    incorrect_answer_two = ""
    provider = ""
    url = ""
    attribution = ""

    def _get_title(self, data=None, **kwargs):
        return u''

    def _get_desc(self, data=None, **kwargs):
        return u''

    def _get_photo_url(self, data=None, **kwargs):
        return None

    def _get_incorrect_answers(self, data=None, **kwargs):
        return (u'Inco 1',u'Inco 2')

    def _get_provider(self, data=None, **kwargs):
        return self.provider

    def _get_url(self, data=None, **kwargs):
        return u''

    def _get_attribution(self, data=None, **kwargs):
        return u''

    def to_dict(self):
        return {'title': self.title,
                'desc': self.desc,
                'photo_url': self.photo_url,
                'correct_answer': self.correct_answer.strip(),
                'incorrect_answer_one': self.incorrect_answer_one.strip(),
                'incorrect_answer_two': self.incorrect_answer_two.strip(),
                'provider': self.provider,
                'url': self.url,
                'attribution': self.attribution,
                'qtype': self.qtype,
                'revised': True,         
                }

    def is_ok_to_submit(self):
        if self.title and self.correct_answer and self.incorrect_answer_one and self.incorrect_answer_two and self.qtype:
            if len(set((self.correct_answer, self.incorrect_answer_one, self.incorrect_answer_two))) ==3:
                return True
            else:
                return False
        else:
            return False
    
    def submit(self):
        if self.is_ok_to_submit():
            payload = self.to_dict()
            headers = {'Authorization': 'Token ' + TRIBUAL_TOKEN,
                   'content-type': 'application/json'}            
            requests.post(API_BASE_URL, data=json.dumps(payload), headers=headers)
            a = 1

        else:
            print 'Ez dago galdera bidaltzeko adina informazioa: ', self.title
        
    def create(self,obj,**kwargs):
        self.title = self._get_title(obj, **kwargs)
        self.photo_url = self._get_photo_url(obj, **kwargs)
        self.correct_answer = self._get_correct_answer(obj, **kwargs)
        self.incorrect_answer_one, self.incorrect_answer_two = self._get_incorrect_answers(obj,**kwargs)
        self.provider = self._get_provider(obj, **kwargs)
        self.url = self._get_url(obj, **kwargs)
        self.attribution = self._get_attribution(obj, **kwargs)
        if 'qtype' in kwargs.keys():
            self.qtype = kwargs.get('qtype')

    def print_galdera(self):
        #for debbug purposses
        if self.is_ok_to_submit():
            photo_url = self.photo_url or u''
            incorrect_one = self.incorrect_answer_one or u''
            incorrect_two = self.incorrect_answer_two or u''
            attribution = self.attribution or u''
            provider = self.provider or u''
            url = self.url or u''
            try:
                print self.title + u"\n\t" + photo_url +\
                  u"\n\t" + self.correct_answer +\
                  u"\n\t" + unicode(incorrect_one) +\
                  u"\n\t" + unicode(incorrect_two) +\
                  u"\n\t" + unicode(attribution) + " at " + unicode(provider) +\
                  u"\n\t" + unicode(url)
            except:
                pass
        else:
            print 'Hau ez da bidaliko...'          
