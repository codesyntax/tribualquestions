import random
import requests
import time
#from tribualserver.utils.load_images import loadUrlImage

from pygeocoder import Geocoder

#from tribualserver.createquestions.utils import create_question, get_lat_lng_square_google

BIZTANLE_KOP_MINIMOA = 250
PANORAMIO_URL = 'http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=%s&minx=%s&miny=%s&maxx=%s&maxy=%s&size=medium&mapfilter=false'

def get_lat_lng_square_google(text):
    """ """
    try:
        h = Geocoder.geocode(text)
    except:
        pass
        return None

    if h.data:
        data = h.data[0]
        if 'geometry' in data.keys():
            geo = data['geometry']
            if 'bounds' in geo.keys():
                bounds = geo['bounds']
                sw = bounds['southwest']
                ne = bounds['northeast']
                return (sw['lng'],ne['lng'], sw['lat'], ne['lat'])
    return None                

def get_count_photos_biztanleak(biztanleak):
    tramoak = ((0,500,2),
               (500,1000,3),
               (1000,2000,5),
               (2000,5000,8),
               (5000,10000,12),
               (10000,100000,18),
               (100000,10000000,25),
              )
    for tramoa in tramoak:
        if biztanleak > tramoa[0] and biztanleak <= tramoa[1]:
            return tramoa[2]
    return 3


def get_panoramio_photos(minx, maxx, miny, maxy,number):
    """ """
    url = PANORAMIO_URL % (str(number),str(minx),str(miny),str(maxx),str(maxy))
    obj = requests.get(url)
    js = obj.json()
    photos = js['photos']
    return photos[:number]


