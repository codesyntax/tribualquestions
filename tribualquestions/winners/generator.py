import unicodecsv
import random

from tribualquestions.geo.generators import GeoGenerator


class WinnerGenerator(GeoGenerator):
    qtype = u'Who won'

def _load_winners(path):
    data = []
    winners = set()
    with open(path, 'r') as f:
        r = unicodecsv.reader(f, encoding='utf-8')
        for event,year,winner in r:
            data.append((event, year, winner))
            winners.add(winner)
    return data, winners

def _get_incorrects(correct, incorrect_set):
    incorrect_copy = incorrect_set.copy()
    incorrect_copy.remove(correct)
    incorrect_list = list(incorrect_copy)
    random.shuffle(incorrect_list)
    return incorrect_list[:2]

def createWinnerQuestions(path):
    file_data, winners = _load_winners(path)
    for current in file_data:
        title = u' '.join((current[0], current[1]))
        correct = current[2]
        incorrect = _get_incorrects(correct, winners)
        generator = WinnerGenerator()
        generator.create(obj=None, **locals())
        generator.submit()
        generator.print_galdera()
