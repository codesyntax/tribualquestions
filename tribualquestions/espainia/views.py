# -*- coding: utf-8 -*-
from tribualquestions.espainia.models import Municipio
from tribualquestions.generator import Generator
from time import sleep
import random

def get_incorrect_municipios(municipio):
    """
    Beste bi herri, antzeko tamainakoak, baina beste Autonomia Erkidego batekoak
    """
    biztanleak = municipio.biztanle_kop
    biz_min = int(biztanleak*1.0 / 2 )
    biz_max = int(biztanleak*2)

    municipios = Municipio.objects.filter(biztanle_kop__range=(biz_min,biz_max)).exclude(autonomi_erki=municipio.autonomi_erki).values_list('izena',flat=True).order_by('?')[:2]
    if len(municipios)==2:
        return municipios
    else:
        #marjena haunditu...
        municipios = Municipio.objects.filter(biztanle_kop__gt=biztanleak).exclude(autonomi_erki=municipio.autonomi_erki).values_list('izena',flat=True).order_by('biztanle_kop')[:2]
        if len(municipios)==2:
            return municipios
        else:
            municipios = Municipio.objects.filter(biztanle_kop__lt=biztanleak).exclude(autonomi_erki=municipio.autonomi_erki).values_list('izena',flat=True).order_by('-biztanle_kop')[:2]            
            if len(municipios)==2:
                return municipios
        return ('','')

class HerriaMapaGenerator(Generator):
    qtype = u'Municipio en España (Google Maps)'
    provider = u'Google Maps'
    attribution = u'Google Maps'

    def _get_photo_url(self, municipio, **kwargs):
        s = 'http://maps.googleapis.com/maps/api/staticmap?center=40.4166909,-3.7003454&zoom=5&size=400x350&maptype=satellite&markers=color:red|label:X|%s,%s&sensor=false' % (municipio.latitudea, municipio.longitudea)
        return s

    def _get_title(self, municipio,**kwargs):
        return u'Municipio'

    def _get_correct_answer(self, municipio,**kwargs):
        return municipio.izena
    
    def _get_incorrect_answers(self, municipio,**kwargs):
        return get_incorrect_municipios(municipio)

    def _get_url(self,municipio,**kwargs):
        s = 'https://www.google.com/maps/place/@%s,%s,10z' % (municipio.latitudea, municipio.longitudea)
        return s


class PanoramioGenerator(Generator):
    qtype = u'Fotos de municipios (España, Panoramio)'
    provider = u'Panoramio'

    def _get_attribution(self, municipio, **kwargs):
        photo = kwargs['photo']        
        return photo[u'owner_name'] + u' (Panoramio)'

    def _get_photo_url(self, municipio, **kwargs):
        photo = kwargs['photo']        
        return photo[u'photo_file_url']

    def _get_title(self, municipio, **kwargs):
        return u'Municipio'

    def _get_correct_answer(self, municipio, **kwargs):
        return municipio.izena
    
    def _get_incorrect_answers(self, municipio, **kwargs):
        return get_incorrect_municipios(municipio)

    def _get_url(self, municipio, **kwargs):
        photo = kwargs['photo']                
        return photo[u'photo_url']



def sortu_googlemaps_galderak():
    municipios = Municipio.objects.filter(biztanle_kop__gt=10000).exclude(autonomi_erki='CANARIAS').order_by('-biztanle_kop')
    for m in municipios:
        g = HerriaMapaGenerator()
        g.create(m)
        g.print_galdera()
        g.submit()
        sleep(4)


from tribualquestions.panoramio.get_photos import get_lat_lng_square_google, get_count_photos_biztanleak, get_panoramio_photos

def sortu_panoramio_galderak():
    municipios = Municipio.objects.filter(biztanle_kop__gt=30000)
    for m in municipios:
        bounds = get_lat_lng_square_google(m.get_name_to_googlegeocoder())
        if bounds:
            minx, maxx, miny, maxy = bounds
            number = get_count_photos_biztanleak(m.biztanle_kop)
            photos = get_panoramio_photos(minx, maxx, miny, maxy, number)
            if photos:
                for photo in photos:
                    g = PanoramioGenerator()
                    g.create(obj=m, photo=photo)
                    g.print_galdera()
                    g.submit()
                    sleep(1)


def get_incorrect_provincias(municipio):
    """
    Beste bi probintzia, antzeko tamainakoak, baina beste Autonomia Erkidego batekoak
    """
    probintziak = Municipio.objects.all().exclude(probintzia=municipio.probintzia).values_list('probintzia',flat=True).distinct().order_by('?')[:2]    
    if len(probintziak)==2:
        return probintziak
    else:
        return ('','')

class QueProvinciaGenerator(Generator):
    qtype = u'Municipios en España (preguntas generales)'
    provider = u''

    def _get_title(self, municipio, **kwargs):
        return municipio.izena

    def _get_correct_answer(self, municipio, **kwargs):
        return municipio.probintzia
    
    def _get_incorrect_answers(self, municipio, **kwargs):
        return get_incorrect_provincias(municipio)


def sortu_probintzia_galderak():
    municipios = Municipio.objects.filter(biztanle_kop__gt=10000).order_by('-biztanle_kop')
    for m in municipios:
        if m.izena.lower()!=m.probintzia.lower():
            g = QueProvinciaGenerator()
            g.create(m)
            g.print_galdera()
            g.submit()
            sleep(1)



def get_incorrect_answers_biztanle_tramoak(biztanleak):
    tramoak = ((0,250,'Menos de 250'),
               (250,1000,'250-1000'),
               (1000,5000,'1.000-5.000'),
               (5000,10000,'5.000-10.000'),
               (10000,20000,'10.000-20.000'),
               (20000,50000,'20.000-50.000'),
               (50000,100000,'50.000-100.000'),
               (100000,150000,'100.000-150.000'),
               (150000,200000,'150.000-200.000'),
               (200000,500000,'200.000-500.000'),
               (500000,1000000,'500.000-1.000.000'),               
               (1000000,2000000,'1.000.000-2.000.000'),                              
               (2000000,20000000000,'Más de 2.000.000'),                                             
              )
    tramo_ona = 0
    t = 0
    for tramoa in tramoak:
        if biztanleak >= tramoa[0] and biztanleak <=tramoa[1]:
            break
        t += 1

    if t==0:
        return (tramoa, tramoak[1], tramoak[2])
    elif t==1:        
        i = random.randint(1,2)
        if i==1:
            return (tramoa, tramoak[t+1], tramoak[t+2])
        else:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
    elif t==len(tramoak)-1:        
        return (tramoa, tramoak[t-1], tramoak[t-2])            
    elif t==len(tramoak)-2:        
        i = random.randint(1,2)
        if i==1:
            return (tramoa, tramoak[t-1], tramoak[t-2])
        else:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
    else:
        i = random.randint(1,3)
        if i==1:
            return (tramoa, tramoak[t+1], tramoak[t+2])
        elif i==2:
            return (tramoa, tramoak[t-1], tramoak[t+1])            
        else:
            return (tramoa, tramoak[t-1], tramoak[t-2])            


    return None

class NumeroHabitantesGenerator(Generator):
    qtype = u'Municipios en España (preguntas generales)'
    provider = u''

    def _get_title(self, municipio, **kwargs):
        return u'¿Cuántos habitantes tiene ' + municipio.izena.upper() + u'?'

    def _get_correct_answer(self, municipio, **kwargs):
        return get_incorrect_answers_biztanle_tramoak(int(municipio.biztanle_kop))[0][2]

    def _get_incorrect_answers(self, municipio, **kwargs):
        biz_kops = get_incorrect_answers_biztanle_tramoak(int(municipio.biztanle_kop))
        return [biz_kops[1][2], biz_kops[2][2]]


def sortu_biztanleak_galderak():
    municipios = Municipio.objects.filter(biztanle_kop__gt=1000).order_by('-biztanle_kop')
    for m in municipios:
        g = NumeroHabitantesGenerator()
        g.create(m)
        g.print_galdera()
        g.submit()
        sleep(1)            