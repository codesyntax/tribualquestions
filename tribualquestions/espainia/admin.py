from tribualquestions.espainia.models import Municipio
from django.contrib import admin
from django.db.models import Count

class AutonomiErkiFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Autonomi Erkidegoa'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'autonomi_erki'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        zerrenda = Municipio.objects.all().values_list('autonomi_erki',).annotate(number_of_entries=Count('pk')).distinct().order_by('autonomi_erki')
        to_r = []
        for z in zerrenda:
            to_r.append((z[0], z[0] + u' (' + unicode(z[1]) + u')'))
        return to_r

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value():
            #u = User.objects.get(username=self.value())
            return queryset.filter(autonomi_erki=self.value())


class ProbintziaFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Probintzia'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'probintzia'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        zerrenda = Municipio.objects.all().values_list('probintzia',).annotate(number_of_entries=Count('pk')).distinct().order_by('probintzia')
        to_r = []
        for z in zerrenda:
            to_r.append((z[0], z[0] + u' (' + unicode(z[1]) + u')'))
        return to_r

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value():
            #u = User.objects.get(username=self.value())
            return queryset.filter(probintzia=self.value())

class MunicipiAdmin(admin.ModelAdmin):
    list_display = ('izena', 'autonomi_erki','probintzia', 'biztanle_kop', )
    ordering = ('izena',)
    list_filter = (ProbintziaFilter,AutonomiErkiFilter)    


admin.site.register(Municipio, MunicipiAdmin)   