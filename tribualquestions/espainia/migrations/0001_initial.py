# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('izena', models.CharField(max_length=150, null=True, blank=True)),
                ('autonomi_erki', models.CharField(max_length=150, null=True, blank=True)),
                ('probintzia', models.CharField(max_length=150, null=True, blank=True)),
                ('postakodea', models.IntegerField(default=0, null=True, blank=True)),
                ('helbidea', models.CharField(max_length=150, null=True, blank=True)),
                ('tlf', models.CharField(max_length=15, null=True, blank=True)),
                ('fax', models.CharField(max_length=15, null=True, blank=True)),
                ('email', models.CharField(max_length=150, null=True, blank=True)),
                ('web', models.CharField(max_length=250, null=True, blank=True)),
                ('biztanle_kop', models.IntegerField(default=0, null=True, blank=True)),
                ('longitudea', models.FloatField(default=0, null=True, blank=True)),
                ('latitudea', models.FloatField(default=0, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
