from django.db import models

"""
CREATE TABLE IF NOT EXISTS `t_municipios` (
  `izena` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `autonomi_erki` varchar(150) DEFAULT NULL,
  `probintzia` varchar(150) DEFAULT NULL,
  `pk` int(11) DEFAULT NULL,
  `helbidea` varchar(150) DEFAULT NULL,
  `tlf` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `web` varchar(250) DEFAULT NULL,
  `biztanle_kop` decimal(9,0) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `longitudea` double NOT NULL,
  `latitudea` double NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8169 DEFAULT CHARSET=latin1;
"""


class Municipio(models.Model):
    izena = models.CharField(max_length=150, null=True, blank=True) 
    autonomi_erki = models.CharField(max_length=150, null=True, blank=True)
    probintzia = models.CharField(max_length=150, null=True, blank=True)
    postakodea = models.IntegerField(default=0, null=True, blank=True)  
    helbidea = models.CharField(max_length=150, null=True, blank=True) 
    tlf = models.CharField(max_length=15, null=True, blank=True) 
    fax = models.CharField(max_length=15, null=True, blank=True) 
    email = models.CharField(max_length=150, null=True, blank=True) 
    web = models.CharField(max_length=250, null=True, blank=True) 
    biztanle_kop = models.IntegerField(default=0, null=True, blank=True)  
    longitudea = models.FloatField(default=0, null=True, blank=True)  
    latitudea = models.FloatField(default=0, null=True, blank=True)  


    def get_name_to_googlegeocoder(self):
        """ """
        return self.izena + u', Spain'

    def __unicode__(self):
        return self.izena