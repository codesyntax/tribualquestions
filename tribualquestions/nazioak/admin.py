from tribualquestions.nazioak.models import Continent, Nazioa
from django.contrib import admin


class ContinentAdmin(admin.ModelAdmin):
    list_display = ('code', 'name',)
    ordering = ('code',)

class NazioaAdmin(admin.ModelAdmin):
    list_display = ('continent', 'code_iso_3166','name','name_en', 'population' )
    ordering = ('code_iso_3166',)
    list_filter = ('continent',)

admin.site.register(Continent, ContinentAdmin)    
admin.site.register(Nazioa, NazioaAdmin)    


