from tribualquestions.nazioak.models import Nazioa
from tribualquestions.generator import Generator

def get_google_chart(code_iso, continent_code):
    fixed = {'AF':('-35,-20,40,50','340x400'),
             'AS':('0,30,80,180','400x400'),
             'EA':('0,-130,40,-40','700x300'),
             'EU':('35,-20,70,50','450x400'),
             'HA':('-60,-130,10,-20','600x400'),
             'IA':('10,-130,60,-20','600x400'),
             'OZ':('-60,50,10,180','600x400'),
            }
    fixed_s = fixed[continent_code]
    return 'https://chart.googleapis.com/chart?cht=map:fixed=%s&chs=%s&chf=bg,0,c6eff7&chld=%s&chco=BBBBBB|FF0000&chm=f????????,000000,0,0,10' % (fixed_s[0],fixed_s[1],code_iso)



def get_incorrect_nazioak_by_continent(nazioa):
    """
    Kontinente berdineko beste bi nazio
    """
    nazioak = Nazioa.objects.filter(continent=nazioa.continent).exclude(pk=nazioa.pk).values_list('name',flat=True).order_by('?')[:2]
    return (nazioak[0],nazioak[1])

class NazioMapaGenerator(Generator):
    qtype = u'Nazioa mapa'
    provider = u''

    def _get_photo_url(self, nazioa, **kwargs):
        return get_google_chart(nazioa.code_iso_3166, nazioa.continent.code)

    def _get_title(self, nazioa,**kwargs):
        return u'Zein herrialde da?'

    def _get_correct_answer(self, nazioa,**kwargs):
        return nazioa.name
    
    def _get_incorrect_answers(self, nazioa,**kwargs):
        return get_incorrect_nazioak_by_continent(nazioa)