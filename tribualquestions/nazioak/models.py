from django.db import models

class Continent(models.Model):
    code = models.CharField(max_length=2,unique=True)
    name = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name


class Nazioa(models.Model):
    continent = models.ForeignKey(Continent)
    code_iso_3166 = models.CharField(max_length=2,unique=True)
    name = models.CharField(max_length=255)
    name_en = models.CharField(max_length=255, null=True, blank=True) 
    population = models.IntegerField(default=0, null=True, blank=True)  


    def __unicode__(self):
        return self.name

