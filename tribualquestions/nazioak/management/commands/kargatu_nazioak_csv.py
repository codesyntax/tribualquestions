import csv
from django.core.management.base import BaseCommand
from tribualquestions.nazioak.models import Continent, Nazioa


def get_nazioak(csv_path):
    """ """
    tor = []
    count = 0
    with open(csv_path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            continent_code = row[0]
            cs = Continent.objects.filter(code=continent_code)
            if cs:
                continent = cs[0]
            else:
                continent = Continent()
                continent.code = continent_code
                continent.name = continent_code
                continent.save()

            nazioa = Nazioa()
            nazioa.continent = continent
            nazioa.code_iso_3166 = row[1].strip()
            nazioa.name = row[2].strip()
            nazioa.name_en = row[3].strip()
            nazioa.population = int(row[4].strip())
            nazioa.save()
            count += 1

    return count


class Command(BaseCommand):
    args = '<qnumber>'
    help = 'Zenbat galdera sortu nahi dituzun HERRI bakoitzeko'

    def handle(self, *args, **options):
        if not args:
            print 'Argumentuak behar ditugu: nazioen csv-a (izena, biztanleak, zona)'
            return None
        else:
            csv_path = args[0]
            Nazioa.objects.all().delete()
            i = get_nazioak(csv_path)
            print i
