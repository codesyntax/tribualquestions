from django.db import models
from tribualquestions.utils.names import get_emakumea_al_da

class Writer(models.Model):
    ref_id = models.SlugField(unique=True)
    ref_url = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    photo_url = models.CharField(max_length=255, null=True, blank=True) 
    born_year = models.IntegerField(default=0, null=True, blank=True)  
    born_place = models.CharField(max_length=255, null=True, blank=True) 
    death_year = models.IntegerField(default=0, null=True, blank=True)  
    death_place = models.CharField(max_length=255, null=True, blank=True) 
    emakumea = models.BooleanField(default=False,db_index=True)

    def save(self, *args, **kwargs):
        self.emakumea = get_emakumea_al_da(self.name)
        super(Writer, self).save(*args, **kwargs)

    def book_number(self):
        return self.book_set.all().count()

    def __unicode__(self):
        return self.name

class Book(models.Model):
    writer = models.ForeignKey(Writer)
    title = models.CharField(max_length=255)
    btype = models.CharField(max_length=255, null=True, blank=True)
    year = models.IntegerField(default=0, null=True, blank=True)  
    editorial = models.CharField(max_length=255, null=True, blank=True) 

    def __unicode__(self):
        return self.title + u' (' + unicode(self.writer.name) + ', %d)' % self.year

    class Meta:
        ordering = ('year',)