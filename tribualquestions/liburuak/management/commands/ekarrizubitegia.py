"""
Hau erabili aurretik mesedez jarri kontaktuak Armiarmako arduradunekin.
Baimena eskatu eta emango zaion erabilpena azaltzeko
"""

from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
import urllib2
import time
import re

from tribualquestions.liburuak.models import Writer, Book

ZUBITEGIA_BASE_URL = u'http://zubitegia.armiarma.com/?i=%d'
IMG_BASE_URL = u'http://zubitegia.armiarma.com'

class Command(BaseCommand):
    args = '<qnumber>'
    help = 'Armiarmaren idazle eta liburuak ekartzeko komandoa'

    def handle(self, *args, **options):
        if not args:
            print 'Argumentuak behar ditugu: azken ID balidoa'
            return None
        else:
            first_id = int(args[0])
            last_id = int(args[1])



        for id in range(first_id,last_id+1):
            url = ZUBITEGIA_BASE_URL % id
            try:
                html = urllib2.urlopen(url).read()
                soup = BeautifulSoup(html)
            except:
                print 'Errorea egon da ID hau duen orria ekarri eta parseorako prestatzen: %s' % id
                continue

            auto = soup.find("p", {"class": "auto"})
            if not auto:
                print 'Errorea %d honekin: ez du izenik' % id
                continue
            izena = auto.text.strip()

            datak = soup.find_all("p", {"class": "jaioHil2"})
            lekuak = soup.find_all("p", {"class": "jaioHil4"})

            jaio_urtea = None
            jaio_lekua = ''
            hil_urtea = None
            hil_lekua = ''

            data_count = 1
            for data in datak:
                if data:
                    if data_count==1:
                        try:
                            jaio_urtea = int(data.text[:4])
                        except:
                            jaio_urtea = None
                    elif data_count==2:
                        try:
                            hil_urtea = int(data.text[:4])
                        except:
                            hil_urtea = None
                data_count += 1

            data_count = 1
            for lekua in lekuak:
                if lekua:
                    if data_count==1:
                        jaio_lekua = lekua.text.split(' - ')[0]
                    elif data_count==2:
                        hil_lekua = lekua.text.split(' - ')[0]
                data_count += 1

            argazkia = soup.find("div", {"id": "autoArgazkia"})
            egilea_src = argazkia.find('img')['src']

            writer = Writer()
            writer.ref_id = id
            writer.ref_url = url
            writer.name = izena
            writer.born_year = jaio_urtea
            writer.born_place = jaio_lekua
            writer.death_year = hil_urtea
            writer.death_place = hil_lekua
            writer.photo_url = IMG_BASE_URL + egilea_src
            writer.save()

            bibliografia = soup.find("div", {"class": "bibliografia"})
            sailak = bibliografia.find_all("div", {"class": "saila"})
            for saila in sailak:
                mota = saila.find("p", {"class":"sailBurua"}).text
                for liburua in saila.find_all("div", {"class": "sailAlea"}):
                    lib_titulua = liburua.find("p", {"class": "sailAlea1"}).text
                    lib_urtea_eta_argitaletxea = liburua.find("p", {"class": "sailAlea2"}).text
                    lib_datuak = lib_urtea_eta_argitaletxea.split(',')
                    if len(lib_datuak)==2:
                        lib_urtea = lib_urtea_eta_argitaletxea.split(',')[0][:4]
                        lib_argitaletxea = lib_urtea_eta_argitaletxea.split(',')[1].strip()
                        book = Book()
                        book.writer = writer
                        book.title = lib_titulua
                        try:
                            book.year = int(lib_urtea)
                        except:
                            book.year = None
                        book.editorial = lib_argitaletxea
                        book.btype = mota
                        book.save()
                        print id, writer.name, mota, lib_urtea, lib_titulua, lib_argitaletxea
            time.sleep(6)

        return None
