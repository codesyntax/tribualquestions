from tribualquestions.liburuak.models import Writer, Book
from django.contrib import admin


class WriterAdmin(admin.ModelAdmin):
    list_display = ('name', 'born_year','born_place', 'death_year', 'death_place', 'book_number','emakumea')
    ordering = ('name',)
    list_filter = ('emakumea',)    

class BtypeFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'BType'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'btype'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        btypes = Book.objects.all().values_list('btype',flat=True)
        u_btypes = list(set(btypes))
        u_btypes.sort()
        tof = []
        for btype in u_btypes:
            tof.append((btype,btype))
        return tof

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value():
            #u = User.objects.get(username=self.value())
            return queryset.filter(btype=self.value())

class BookAdmin(admin.ModelAdmin):
    list_display = ('writer', 'title', 'year','btype', 'editorial')
    ordering = ('writer','year','title')
    list_filter = (BtypeFilter,)

admin.site.register(Writer, WriterAdmin)    
admin.site.register(Book, BookAdmin)    


