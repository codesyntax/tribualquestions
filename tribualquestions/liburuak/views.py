from tribualquestions.generator import Generator
from tribualquestions.liburuak.models import Book, Writer
import random
from datetime import datetime

def get_incorrect_writers_by_book(book,sex=False):
    """
    Liburu horren estilo eta garai bertsuko beste bi egile saiatuko naiz ekartzen
    Garaikorik ez bada, edozein garaikoak
    Eez bada, batez
    """
    if sex:
        writers = Book.objects.filter(writer__emakumea=book.writer.emakumea,btype=book.btype, year__range=(book.year-10,book.year+10)).exclude(writer=book.writer).values_list('writer',flat=True)
    else:
        writers = Book.objects.filter(btype=book.btype, year__range=(book.year-10,book.year+10)).exclude(writer=book.writer).values_list('writer',flat=True)        
    writers = list(set(writers))
    if len(writers)>1:
        random.shuffle(writers)
        egilea_1 = Writer.objects.get(pk=writers[0]).name
        egilea_2 = Writer.objects.get(pk=writers[1]).name
        return (egilea_1, egilea_2)

    writers = Book.objects.filter(btype=book.btype).exclude(writer=book.writer).values_list('writer',flat=True)
    writers = list(set(writers))
    if len(writers)>1:
        writers2 = list(set(writers))
        random.shuffle(writers)
        egilea_1 = Writer.objects.get(pk=writers[0]).name
        egilea_2 = Writer.objects.get(pk=writers[1]).name
        return (egilea_1, egilea_2)

    return (u'', u'')

def get_incorrect_writers(writer):
    """
    Egile baten antzeko beste bi...
    egingo duguna da, goiko metodoa baliatu, idazle honen liburua pasata
    """
    books = writer.book_set.all().order_by('?')
    if books:
        return get_incorrect_writers_by_book(books[0],sex=True)
    else:
        return (u'', u'')


def get_diff_years(year=0):
    tramoak = ((0,2,1,2),
               (2,5,2,3),
               (5,10,4,7),
               (10,40,8,12),               
               (40,100,16,24),
               (100,200,40,60),
               (200,10000,80,120)
              )
    diff_years = datetime.now().year - year
    for tramoa in tramoak:
        if diff_years >= tramoa[0] and diff_years < tramoa[1]:
            return (tramoa[2],tramoa[3])
    return (0,0)

def get_incorrect_years(year):
    """
    Liburua berri xamarra bada, + edo - 5 urte, aleatorioki
    Liburua zahar xamarra bada, + edo - 10 urte, aleatorioki
    Bestela, + - 50 urte aleatorioki 
    """
    result = year
    this_year = datetime.now().year
    diff_years = get_diff_years(result)

    if result + diff_years[1] > this_year:
        i = 3
    elif result + 2* diff_years[1] > this_year:
        i = random.randint(2,3)                
    else:
        i = random.randint(1,3)        

    if i==1:
        okerra1 = result + random.randint(diff_years[0],diff_years[1])
        okerra2 = okerra1 + random.randint(diff_years[0],diff_years[1])
        okerrak = (okerra1, okerra2)
    elif i==2:
        okerra1 = result + random.randint(diff_years[0],diff_years[1])
        okerra2 = result - random.randint(diff_years[0],diff_years[1])
        okerrak = (okerra1, okerra2)
    else:
        okerra1 = result - random.randint(diff_years[0],diff_years[1])
        okerra2 = okerra1 - random.randint(diff_years[0],diff_years[1])
        okerrak = (okerra1, okerra2)

    return (unicode(okerrak[0]), unicode(okerrak[1]))


def _get_writer_born_places():
    """ """
    places = Writer.objects.filter(born_place__isnull=False).exclude(born_place='').values_list('born_place',flat=True)
    places = list(places)
    u_places = dict((i,places.count(i)) for i in places)
    l_places = []
    for k,v in u_places.items():
        l_places.append({'born_place':k,'count':v})

    l_places = sorted(l_places, key=lambda k: -k['count'])
    return l_places

def get_incorrect_place(place_name,places=None):
    """
    Antzeko idazle kopurua duen beste herri batzuk eman 
    """
    if not places:
        places = _get_writer_born_places()

    i = 0
    for tplace in places:
        if tplace['born_place']==place_name:
            place_to_pop = tplace
            break
        i += 1

    indizea = i
    maxindizea = min(len(places), indizea+4)
    minindizea = max(0,indizea-4)
    places_filtratuak = places[minindizea:maxindizea]
    places_filtratuak.pop(places_filtratuak.index(tplace))
    random.shuffle(places_filtratuak)
    return (places_filtratuak[0]['born_place'], places_filtratuak[1]['born_place'])

class ZeinDaEgileaGenerator(Generator):
    qtype = u'Zubitegia'
    provider = u'Armiarma'

    def _get_title(self, book):
        return u'Nork idatzi zuen "%s" liburua %d. urtean?' % (book.title, book.year)

    def _get_desc(self, book=None):
        return u''

    def _get_photo_url(self, book=None):
        return None

    def _get_correct_answer(self, book):
        return book.writer.name
    
    def _get_incorrect_answers(self, book,):        
        return get_incorrect_writers_by_book(book)

    def _get_url(self, book):
        return book.writer.ref_url

    def _get_attribution(self, book):
        return u'Armiarma'

class NoizkoLiburuaDa(Generator):
    qtype = u'Liburua noiz?'
    provider = u'Armiarma'

    def _get_title(self, book):
        return u'Noiz idatzi zuen %s idazleak "%s" liburua' % (book.writer.name, book.title)

    def _get_desc(self, book=None):
        return u''

    def _get_photo_url(self, book=None):
        return None

    def _get_correct_answer(self, book):
        return unicode(book.year)
    
    def _get_incorrect_answers(self, book,):        
        return get_incorrect_years(book)

    def _get_url(self, book):
        return book.writer.ref_url

    def _get_attribution(self, book):
        return u'Armiarma'

class NoizJaioZen(Generator):
    qtype = u'Noiz jaio zen idazlea?'
    provider = u'Armiarma'

    def _get_title(self, writer):
        return u'Noiz jaio zen %s idazlea (%s)?' % (writer.name,writer.born_place)

    def _get_desc(self, writer=None):
        return u''

    def _get_photo_url(self, writer=None):
        return None

    def _get_correct_answer(self, writer):
        return unicode(writer.born_year)
    
    def _get_incorrect_answers(self, writer):        
        return get_incorrect_years(writer.born_year)

    def _get_url(self, writer):
        return writer.ref_url

    def _get_attribution(self, writer):
        return u'Armiarma'


class NonJaioZen(Generator):
    qtype = u'Non jaio zen idazlea?'
    provider = u'Armiarma'

    def _get_title(self, writer):
        return u'Non jaio zen %s idazlea, %d. urtean?' % (writer.name,writer.born_year)

    def _get_desc(self, writer=None):
        return u''

    def _get_photo_url(self, writer=None):
        return None

    def _get_correct_answer(self, writer):
        return writer.born_place
    
    def _get_incorrect_answers(self, writer):        
        return get_incorrect_place(writer.born_place)

    def _get_url(self, writer):
        return writer.ref_url

    def _get_attribution(self, writer):
        return u'Armiarma'


class NorDaArgazkikoIdazlea(Generator):
    qtype = u'Nor da idazlea?'
    provider = u'Armiarma'

    def _get_title(self, writer):
        return u'Euskal idazlea'

    def _get_desc(self, writer=None):
        return u''

    def _get_photo_url(self, writer=None):
        return writer.photo_url

    def _get_correct_answer(self, writer):
        return writer.name
    
    def _get_incorrect_answers(self, writer):        
        return get_incorrect_writers(writer)

    def _get_url(self, writer):
        return writer.ref_url

    def _get_attribution(self, writer):
        return u'Armiarma'