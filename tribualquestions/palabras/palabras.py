import random
import string

from tribualquestions.hiztegibatua.hitza_asmatu import AsmatuHitzaGenerator
from tribualquestions.hiztegibatua.views import OrdenatuEtaOsatuHitzGenerator

def load_file(file_path, min_length=None, max_length=None):
    if min_length is None:
        min_length = 0
    if max_length is None:
        max_length = 1000 
    word_list = []
    with open(file_path) as f:
        for line in f.readlines():
            data = line.split('\t')
            if data:
                word = data[0]
                if min_length < len(word) <= max_length:
                    word_list.append(unicode(data[0], 'utf8'))
    return word_list

def create_match_word(file_path, luzera_min,luzera_max,qtype):
    # Nagusientzako len(hitza) >= 8
    # Umeentzako 5 <= len(hitza) <= 8
    izenak = load_file(file_path, luzera_min, luzera_max)
    ag = AsmatuHitzaGenerator()
    ag.provider = 'RAE'
    ag.qtype = qtype
    for izena in izenak:
        datuak = {} 
        izena = izena
        pos = int(len(izena)/3)
        i = random.randint(pos,len(izena)-pos)
        try:
            galdera = u'{0}{1}{2}'.format(u' _ ' * i, unicode(izena[i]).upper(), u' _ ' * (len(izena) - i -1 ))
        except Exception, e:
            import pdb;pdb.set_trace()
            
        galdera = galdera.strip()
        luzera = len(izena)
        letra = unicode(izena[i])
        okerrak = ag.get_okerrak(izenak,luzera,i,izena[i])

        if okerrak and letra in string.ascii_letters:
            datuak['zuzena'] = izena.upper()
            datuak['galdera'] = galdera
            datuak['okerra1'] = okerrak[0].upper()
            datuak['okerra2'] = okerrak[1].upper()
            try:
                datuak['url'] = ag.BASE_URL % {'search': unicode(izena)}
            except:
                import pdb;pdb.set_trace()
                
                print 'Errorea unicode galdera sortzerakoan', izena
                
    
            ag.create(datuak, qtype=qtype)
            ag.submit()
            ag.print_galdera()
                                    
def create_unordered(file_path, luzera_min,luzera_max,qtype):
    # Nagusientzako len(hitza) >= 8
    # Umeentzako 5 <= len(hitza) <= 8
    izenak = load_file(file_path, luzera_min, luzera_max)
    ag = OrdenatuEtaOsatuHitzGenerator()
    ag.provider = 'RAE'
    ag.qtype = qtype
    ag.attribution = 'RAE'
    for izena in izenak:
        ag.create(izena, hitzak=izenak)
        ag.submit()
