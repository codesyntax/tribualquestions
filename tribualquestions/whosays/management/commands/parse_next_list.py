from tribualquestions.whosays.views import parse_twitter_list, create_question_from_twitter_list
from tribualquestions.whosays.models import List
from django.core.management.base import BaseCommand 
from time import sleep
from random import randint

class Command(BaseCommand):

    def handle(self, *args, **options):
        tl = List.objects.filter(parsing=False).order_by('parsed')[0]
        print 'Start list parsing ', tl.get_title()
        tl.parsing = False #True
        tl.save()
        parse_twitter_list(tl)
        tl.parsing = False
        tl.save()
        print 'End list parsing ', tl.get_title()       
        print 'Start creating questions'
        create_question_from_twitter_list(tl)
        print 'End process'
