from tribualquestions.whosays.models import List, Status, UserList
from django.contrib import admin


class ListAdmin(admin.ModelAdmin):
    list_display = ('owner_screen_name', 'slug','valid_language', 'status_number')
    ordering = ('owner_screen_name',)

class StatusAdmin(admin.ModelAdmin):
    list_display = ('is_valid', 'is_processed','created_at','user_name','text', )
    ordering = ('-created_at',)
    list_filter = ('list','is_valid', 'is_processed')

class UserListAdmin(admin.ModelAdmin):
    list_display = ('user_screen_name', 'user_group_info','list', )
    ordering = ('list','user_screen_name')
    list_filter = ('list',)


admin.site.register(List, ListAdmin)    
admin.site.register(Status, StatusAdmin)
admin.site.register(UserList, UserListAdmin)        


