import csv
from tribualquestions.whosays.models import UserList


def get_twitter_users(csv_path, list):
    """ """
    tor = []
    count = 0
    with open(csv_path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            ul = UserList()
            ul.list = list

            ul.user_screen_name = row[0]
            ul.user_group_info = row[1]
            ul.save()

            count += 1

    return count

