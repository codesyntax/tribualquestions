from django.db import models
from django.db.models import Count, Max, Min
from tribualquestions.utils.tweepy_utils import get_tweepy_user


class List(models.Model):  
    """ """   
    owner_screen_name = models.CharField(max_length=150)
    slug = models.CharField(max_length=150)

    since_id = models.BigIntegerField(null=True,blank=True)
    max_id = models.BigIntegerField(null=True,blank=True)

    valid_language = models.CharField(max_length=2)
    valid_length_min = models.IntegerField(default=50)
    valid_length_max = models.IntegerField(default=300)
    valid_entities = models.BooleanField(default=False)

    parsed = models.DateTimeField(null=True,blank=True, db_index=True)
    parsing = models.BooleanField(default=False,db_index=True)
    parsed_error = models.PositiveSmallIntegerField(default=0)

    added = models.DateTimeField(auto_now_add=True)

    def get_title(self):
        return self.owner_screen_name + u'/' + self.slug    

    def status_number(self):
        return self.status_set.all().count()

    class Meta:
        verbose_name = 'Twitter List'
        verbose_name_plural = 'Twitter Lists' 

    def __unicode__(self):
        return self.owner_screen_name + u'/' + self.slug


ST_TYPE_CHOICES = ((0,'Normal'),(1,'RT'),(2,'Reply'))
class Status(models.Model):  
    """ """    
    list = models.ForeignKey(List)
    twitter_id = models.BigIntegerField(unique=True, db_index=True) 
    user_id = models.BigIntegerField(db_index=True)       
    user_screen_name = models.CharField(null=True,blank=True, max_length=255)
    user_name = models.CharField(null=True,blank=True, max_length=255)    
    stype = models.PositiveSmallIntegerField(choices = ST_TYPE_CHOICES, default = 0, db_index=True)
    language = models.CharField(max_length=3,db_index=True)
    domain = models.CharField(null=True,blank=True, max_length=255, db_index=True)
    url = models.CharField(max_length=255, db_index=True) #Status bakoitzeko, bakarra
    created_at = models.DateTimeField(db_index=True)
    created_at_unixtime =  models.IntegerField(db_index=True)
    created_at_year =  models.PositiveIntegerField(default = 0,  db_index=True) #2014
    created_at_month =  models.PositiveIntegerField(default = 0,   db_index=True) #201401
    created_at_week = models.PositiveIntegerField(default = 0,   db_index=True) #201402
    text = models.CharField(max_length=255)
    is_valid = models.BooleanField(default=False,db_index=True)    
    is_processed = models.BooleanField(default=False,db_index=True)    

    class Meta:
        verbose_name = 'Twitter List Status'
        verbose_name_plural = 'Twitter List Statuses' 

    def __unicode__(self):
        return self.text

class UserList(models.Model):
    """ """
    list = models.ForeignKey(List)
    user_screen_name = models.CharField(max_length=255)
    user_group_info = models.CharField(max_length=255)    

    class Meta:
        verbose_name = 'Twitter User in List'
        verbose_name_plural = 'Twitter Users in List' 

    def __unicode__(self):
        return self.user_screen_name + u' (' + self.user_group_info + u')'
