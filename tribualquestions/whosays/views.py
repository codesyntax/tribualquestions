from datetime import datetime

from tribualquestions.whosays.models import Status, List, UserList
from tribualquestions.whosays.generator import WhoSaysGenerator

from tribualquestions.utils.tweepy_utils import get_tweepy_api, TweepError
from tribualquestions.utils.datetime_utils import get_epoch_seconds
from tribualquestions.utils.language_utils import get_language_id_in_our_list_from_tweet
from django.db.models import Max, Count, Sum


TIMELINE_MAX = 200

def parse_twitter_list(tl=None):
    """ """
    api = get_tweepy_api()

    since_id = tl.since_id and tl.since_id + 1  or None
    max_id = tl.max_id and tl.max_id -1 or None

    try:
        timeline = api.list_timeline(owner_screen_name=tl.owner_screen_name,slug=tl.slug,  count=TIMELINE_MAX, include_rts=False, include_entities=False, since_id = since_id, max_id = max_id)
    except:
        tl.parsed = datetime.now()
        tl.parsed_error = tl.parsed_error + 1 
        tl.save()
        #inprimatu logean
        return None

    save_timeline(timeline,tl)

    if len(timeline):
        #ez nahiz mugara iritsi behetik
        tl.max_id = timeline[-1].id
        tl.save()
    else:
        #mugara iritsi naiz
        max_st_id = Status.objects.filter(list=tl).aggregate(Max('twitter_id')).get('twitter_id__max',None)
        tl.since_id = max_st_id
        tl.max_id = None
        tl.parsed = datetime.now()
        tl.parsed_error = 0
        tl.save()



def is_this_text_valid(text, language, tl):
    if language != tl.valid_language:
        return False
    if len(text) < tl.valid_length_min or len(text) > tl.valid_length_max:
        return False
    if text.find(u'@') > -1:
        return False
    if text.find(u'#') > -1:
        return False
    if text.find(u'http') > -1:
        return False
    return True


def save_timeline(timeline,tl):
    """ """
    sts = []
    for obj in timeline:
        st = Status()
        st.list = tl
        st.twitter_id = obj.id
        st.user_id = obj.user.id
        st.user_screen_name = obj.user.screen_name
        st.user_name = obj.user.name
        if hasattr(obj,'retweeted_status'):
            st.stype = 1
        elif obj.in_reply_to_status_id:
            st.stype = 2
        else:
            st.stype = 0
        st.language = get_language_id_in_our_list_from_tweet(obj.text)
        st.created_at = obj.created_at
        st.created_at_unixtime = get_epoch_seconds(st.created_at)
        st.created_at_year = st.created_at.year
        st.created_at_month = st.created_at.year * 100 + st.created_at.month 
        st.created_at_week = st.created_at.year * 100 + st.created_at.isocalendar()[1]
        st.text = obj.text #.encode('ascii','ignore').decode('ascii')[:255]
        st.is_valid = is_this_text_valid(st.text,st.language,tl)
        if st.is_valid:
            st.is_processed = False
        else:
            st.is_processed = True
        try:
            st.save()
        except:
            print 'Errorea tuit hau gordetzerakoan' , st.twitter_id

    return True


def get_alderdi_okerrak(tl, alderdia):
    alderdiak = UserList.objects.filter(list=tl).exclude(user_group_info=alderdia).values_list('user_group_info',flat=True).order_by('?')
    alderdiak = list(alderdiak)
    okerra1 = alderdiak[0]
    alderdiak = [a for a in alderdiak if a!=okerra1]
    okerra2 = alderdiak[0]
    return okerra1, okerra2

def create_question_from_twitter_list(tl):
    """ """
    sts = Status.objects.filter(is_valid=True, is_processed=False)
    for st in sts:
        uls =  UserList.objects.filter(list = tl, user_screen_name = st.user_screen_name)
        if uls:
            alderdia = uls[0].user_group_info
            okerrak = get_alderdi_okerrak(tl, alderdia)
            data = {'galdera':st.text, 
                    'zuzena':alderdia,
                    'okerra1':okerrak[0],
                    'okerra2':okerrak[1],
                    'url':st.url,
                    'attribution':st.user_name + u' (twitter)'}
            gen = WhoSaysGenerator()
            gen.qtype = tl.slug
            gen.create(data)
            gen.print_galdera()
            gen.submit()

    sts.update(is_processed=True)