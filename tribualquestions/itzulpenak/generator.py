import requests
import re
import json
import random
from bs4 import BeautifulSoup as bs

from tribualquestions.geo.generators import GeoGenerator


"""
Sarrerako fitxategia lortzeko sparql kontsulta. Tripletako lehen filtroa atzitu nahi den
dbpediaren 'Hizkuntza' orria izan behar da

select  ?abstract ?loturak where {dbpedia:Language dbpedia-owl:abstract ?abstract .
                                     dbpedia:Language owl:sameAs ?loturak .
                                      
}

"""
LANGUAGE_MATCHER = re.compile('http://(?P<language>[a-z]*).wikipedia.org(\.*)')
MIN_SENTENCE_LENGTH_IN_WORDS = 7

def get_language_code(url):
    match = LANGUAGE_MATCHER.match(url)
    if match is not None:
        return match.group('language')
    
def remove_html(bstag):
    if bstag is not None:
        return bstag.getText()

def find_text(html):
    if html is not None:
        soup = bs(html, 'html.parser')
        all_p = soup.find_all('p')
        current = u''
        for p_tag in all_p:
            if len(p_tag) > len(current):
                current = p_tag
            if current != u'':
                return remove_html(current)
        
def get_url(url):
    try:
        response = requests.get(url)
    except requests.exceptions.ConnectionError:
        return u''
    if response.status_code == requests.codes.ok:
        return response.text
    
        
def load_urls(data_file_path):
    data_list = []
    language_set = set()
    with open(data_file_path) as f:
        datuak = json.loads(f.read())
        results = datuak.get('results')
        for result in results.get('bindings'):
            original = result.get('loturak').get('value')
            wikipedia_url = re.sub(u'dbpedia', u'wikipedia', original,  re.UNICODE)
            wikipedia_url = re.sub(u'resource', u'wiki', wikipedia_url, re.UNICODE)
            html = get_url(wikipedia_url)
            text = find_text(html)
            if html is not None:
                language_code = get_language_code(wikipedia_url)
                if (language_code is not None) and (text is not None):
                    data_list.append({u'abstract': text,
                                      u'language_code': language_code,
                                      u'url': wikipedia_url})
                    language_set.add(language_code)
    return data_list, language_set

class ItzulpenakGenerator(GeoGenerator):
    qtype = u'What language is'
    
    def _get_title(self, obj, **kwargs):
        return kwargs.get('title')

    def _get_provider(self, data=None, **kwargs):
        return u'Wikipedia'

    def _get_url(self, data=None, **kwargs):
        return kwargs.get('url')
    
    def _get_incorrect_answers(self, obj, **kwargs):
        incorrect = kwargs.get('incorrect')
        correct = kwargs.get('correct')
        if len(incorrect) != 2 or correct in incorrect:
            return ()
            
        return (u'language_' + incorrect[0], u'language_' + incorrect[1])

def get_incorrect_answers(correct, wrong):
    wrong_copy = wrong.copy()
    wrong_copy.remove(correct)
    wrong_copy_list = list(wrong_copy)
    random.shuffle(wrong_copy_list)
    return wrong_copy_list[:2]
    
def create_questions(correct, all_languages):
    abstract = correct.get('abstract')
    correct_answer = correct.get('language_code')
    wikipedia_url = correct.get('url')
    sentences_list = abstract.split(u'.')
    for sentence in sentences_list:
        words = sentence.split(' ')
        if len(words) > MIN_SENTENCE_LENGTH_IN_WORDS:
            data = {u'title': sentence,
                    u'correct': u'language_' + correct_answer,
                    u'incorrect': get_incorrect_answers(correct_answer, all_languages),
                    u'url': wikipedia_url}
            print data
            generator = ItzulpenakGenerator()
            generator.create(obj=None,**data)
            generator.submit()
            generator.print_galdera()

def main(data_file_path):
    correct_list,  all_languages_set = load_urls(data_file_path)
    for correct in correct_list:
        create_questions(correct, all_languages_set)
