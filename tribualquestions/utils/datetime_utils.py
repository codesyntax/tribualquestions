import datetime

def get_epoch_seconds(date=None):
    if date==None:
        date = datetime.datetime.now()
    epoch = datetime.datetime(1970,1,1)
    return (date - epoch).total_seconds()