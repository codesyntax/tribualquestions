import tweepy
from django.conf import settings
from tweepy import TweepError


def get_tweepy_auth():

    consumer_key = getattr(settings,'TWEEPY_CONSUMER_KEY','')
    consumer_secret = getattr(settings,'TWEEPY_CONSUMER_SECRET','')
    access_token = getattr(settings,'TWEEPY_ACCESS_TOKEN','')
    access_token_secret = getattr(settings,'TWEEPY_ACCESS_TOKEN_SECRET','')

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return auth

def get_tweepy_api():
    """ """
    tweepy_auth = getattr(settings,'TWEEPY_AUTH',False)

    if tweepy_auth:
        auth = get_tweepy_auth()
        api = tweepy.API(auth)
    else:
        api = tweepy.API()

    return api

def get_tweepy_user(screen_name):
    """ """
    api = get_tweepy_api()
    u = api.get_user(screen_name)
    return u