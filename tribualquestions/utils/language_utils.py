from langid import langid
import re

from django.conf import settings

LANGID_CONFIDENCE_LIMIT = 0.5
TWSL_LANGUAGE_LIST = getattr(settings,'TWSL_LANGUAGE_LIST',(u'eu',u'es',u'fr',u'en'))


def get_clean_tweet_text(text):
    """ """ 
    username = re.compile('@([a-zA-Z0-9_])+')
    url = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    only_letters = re.compile(u'\W+\'',re.UNICODE)
    hashtag = re.compile('#([a-zA-Z0-9_])+')
    return unicode(only_letters.sub(u' ', url.sub(u'', username.sub(u'',hashtag.sub(u'',text)))))



def get_language_id(text):
    """ """
    langids = langid.classify(text)
    return langids and langids[0] or '-'


def get_language_id_in_our_list_from_tweet(text):
    """ """
    text = get_clean_tweet_text(text)
    our_language_list = TWSL_LANGUAGE_LIST
    langids = langid.classify(text)
    if langids:
        if langids[1] > LANGID_CONFIDENCE_LIMIT and langids[0] in our_language_list:
            return langids[0]
        else:
            return u'ot'

    else:
        return u'ot'
