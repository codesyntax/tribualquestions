from django.conf import settings

EMAKUME_IZENAK_TXT = getattr(settings,'EMAKUME_IZENAK_TXT','/var/csmant/django/tribualquestions/data/emakumeizenak.txt')


def get_emakume_izenak_lista():
    """ """ 
    with open(EMAKUME_IZENAK_TXT, 'r') as f:
        content = f.read().splitlines()
    tor = []
    for a in content:
        try:
            tor.append(unicode(a))
        except:
            pass
    return tor

def get_emakumea_al_da(izena):
    for hasiera in get_emakume_izenak_lista():
        if hasiera.strip() and izena.startswith(hasiera.strip() + u' '):
            return True
    return False

