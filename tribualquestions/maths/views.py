from tribualquestions.generator import Generator
import random
from datetime import datetime
from tribualquestions.maths.numericstringparser import NumericStringParser
import re

class BatuketaBiderketaGenerator(Generator):
    qtype = u'Matematika (umeentzat)'
    provider = u'-'

    def _get_title(self, title):

        return title

    def _get_desc(self, title):
        return u''

    def _get_photo_url(self, title):
        return None

    def _get_correct_answer(self, title):
        result = eval(title)
        return unicode(result)
    
    def _get_incorrect_answers(self, title):        
        result = int(self.correct_answer)
        i = random.randint(1,3)
        if result < 100:
            if i==1:
                okerra1 = result + random.randint(1,9)
                okerra2 = okerra1 + random.randint(1,9)
            elif i==2:
                okerra1 = result - random.randint(1,9)
                okerra2 = okerra1 - random.randint(1,9)
            else:
                okerra1 = result + random.randint(1,9)
                okerra2 = result - random.randint(1,9)
        else:
            if i==1:
                okerra1 = result+10
                okerra2 = result+20
            elif i==2:
                okerra1 = result-10
                okerra2 = result-20
            else:
                okerra1 = result-10
                okerra2 = result+10
        return (unicode(okerra1),unicode(okerra2))


    def _get_url(self, title):
        return ''

    def _get_attribution(self, title):
        return u'-'


def create_math_batuketa(batugai_kopuru_min, batugai_kopuru_max, batugai_balio_min, batugai_balio_max,qtype=None):
    """
    13+11+4+25+2+3+6+6+7
    """
    str = ''
    for i in range(1,random.randint(batugai_kopuru_min,batugai_kopuru_max)):
        str = '%s+%d' % (str,random.randint(batugai_balio_min,batugai_balio_max))
    str = str[1:]        

    q = BatuketaBiderketaGenerator()
    q.create(str,qtype)
    return q        

def create_math_biderketa(batugai_kopuru_min, batugai_kopuru_max, batugai_balio_min, batugai_balio_max,qtype=None):
    """
    7*8*3
    """
    str = ''
    for i in range(1,random.randint(batugai_kopuru_min,batugai_kopuru_max)):
        str = '%s*%d' % (str,random.randint(batugai_balio_min,batugai_balio_max))
    str = str[1:]        

    q = BatuketaBiderketaGenerator()
    q.create(str,qtype)
    return q           


class ZeinDaHaundienaGenerator(Generator):
    qtype = u'Math zatikiak'
    provider = u'-'
    zatikiak = ['0.0','0.0','0.0']
    e_zatikiak = [0.0,0.0,0.0]

    def _get_title(self, zatikiak,**kwargs):
        if len(zatikiak)==3:
            self.zatikiak = zatikiak
            i = 0
            for zatikia in self.zatikiak:
                self.e_zatikiak[i] = eval('1.0 * %s' % self.zatikiak[i])
                i += 1
            if len(set(self.e_zatikiak))==3:
                return u'Zein zenbaki da HAUNDIENA?'
            else:
                return ''
        else:
            return ''


    def _get_correct_answer(self, zatikiak,**kwargs):
        haundiena = max(self.e_zatikiak)
        return self.zatikiak[self.e_zatikiak.index(haundiena)]
    
    def _get_incorrect_answers(self, zatikiak,**kwargs):
        self.zatikiak.pop(self.zatikiak.index(self.correct_answer))      
        return (unicode(self.zatikiak[0]),unicode(self.zatikiak[1]))


class ZeinDaTxikienaGenerator(Generator):
    qtype = u'Math zatikiak'
    provider = u''
    zatikiak = ['0.0','0.0','0.0']
    e_zatikiak = [0.0,0.0,0.0]

    def _get_title(self, zatikiak,**kwargs):
        if len(zatikiak)==3:
            self.zatikiak = zatikiak
            i = 0
            for zatikia in self.zatikiak:
                self.e_zatikiak[i] = eval('1.0 * %s' % self.zatikiak[i])
                i += 1
            if len(set(self.e_zatikiak))==3:
                return u'Zein zenbaki da TXIKIENA?'
            else:
                return ''
        else:
            return ''

    def _get_correct_answer(self, zatikiak,**kwargs):
        haundiena = min(self.e_zatikiak)
        return self.zatikiak[self.e_zatikiak.index(haundiena)]
    
    def _get_incorrect_answers(self, zatikiak,**kwargs):
        self.zatikiak.pop(self.zatikiak.index(self.correct_answer))      
        return (unicode(self.zatikiak[0]),unicode(self.zatikiak[1]))


def get_zatikia(g_min,g_max,b_min,b_max,exclude_b=[]):
    #beti b > g 
    g = random.randint(g_min,g_max)
    b = random.randint(b_min,b_max)
    if g >= b:
        g = random.randint(g_min, b)
    if g==b:
        return ''
    if b in exclude_b:
        return ''
    else:
        return '%d/%d' % (g,b)


def create_zatikiak(g_min,g_max,b_min,b_max,qtype=None):
    """
    Zein da haundiena? 1/2, 2/5, 3/4 
    Zein da txikiena? 1/2, 2/5, 3/4
    """
    exclude_b = []
    z1 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z1:
        return None

    exclude_b.append(int(z1.split('/')[1]))
    z2 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z2:
        return None

    exclude_b.append(int(z2.split('/')[1]))    
    z3 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z3:
        return None

    l = [z1,z2,z3]
    q = ZeinDaHaundienaGenerator()
    if qtype:
        q.create(l,qtype=qtype)
    else:
        q.create(l)
    return q           

def create_zatikiak_txikiena(g_min,g_max,b_min,b_max,qtype=None):
    """
    Zein da haundiena? 1/2, 2/5, 3/4 
    Zein da txikiena? 1/2, 2/5, 3/4
    """
    exclude_b = []
    z1 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z1:
        return None

    exclude_b.append(int(z1.split('/')[1]))
    z2 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z2:
        return None

    exclude_b.append(int(z2.split('/')[1]))    
    z3 = get_zatikia(g_min,g_max,b_min,b_max,exclude_b)
    if not z3:
        return None

    l = [z1,z2,z3]
    q = ZeinDaTxikienaGenerator()
    if qtype:
        q.create(l,qtype=qtype)
    else:
        q.create(l)
    return q          


class EkuazioaGenerator(Generator):
    qtype = u'Math ekuazioak'
    provider = u''
    batugaiak = []
    int_correct_answer = 0

    def _get_title(self, batugaiak,**kwargs):
        self.batugaiak = batugaiak
        emaitza = 0
        eragiketak = [u' + ',u' - ']
        title = u''
        for batu in batugaiak:
            random.shuffle(eragiketak)
            title += eragiketak[0] + unicode(batu)
        title = title[3:]
        emaitza = eval(title)
        #title = u' + '.join([unicode(a) for a in batugaiak])
        title += u' = ' + unicode(emaitza)
        random.shuffle(batugaiak)
        self.int_correct_answer = batugaiak[0]
        return re.sub(r'\b%d\b' % self.int_correct_answer, 'X', title)

    def _get_correct_answer(self, zatikiak,**kwargs):
        return unicode(self.int_correct_answer)
    
    def _get_incorrect_answers(self, title):        
        result = int(self.correct_answer)
        i = random.randint(1,3)
        if result < 100:
            if i==1:
                okerra1 = result + random.randint(1,9)
                okerra2 = okerra1 + random.randint(1,9)
            elif i==2:
                okerra1 = result - random.randint(1,9)
                okerra2 = okerra1 - random.randint(1,9)
            else:
                okerra1 = result + random.randint(1,9)
                okerra2 = result - random.randint(1,9)
        else:
            if i==1:
                okerra1 = result+10
                okerra2 = result+20
            elif i==2:
                okerra1 = result-10
                okerra2 = result-20
            else:
                okerra1 = result-10
                okerra2 = result+10
        return (unicode(okerra1),unicode(okerra2))


    def is_ok_to_submit(self):
        zenbat_x = self.title.split('X')
        if len(zenbat_x) % 2 == 1:
            return False
        else:
            return super(EkuazioaGenerator, self).is_ok_to_submit()        


def create_math_ekuazioa(batugai_kopuru_min, batugai_kopuru_max, batugai_balio_min, batugai_balio_max,qtype=None):
    """
    7*8*3
    """
    batugaiak = []
    for i in range(1,random.randint(batugai_kopuru_min,batugai_kopuru_max)):
        batugaiak.append(random.randint(batugai_balio_min,batugai_balio_max))

    q = EkuazioaGenerator()
    q.create(batugaiak)
    return q   
