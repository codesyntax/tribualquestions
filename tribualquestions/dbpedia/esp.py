# -*- coding: utf-8 -*-
import random
import re
from collections import defaultdict

from tribualquestions.generator import Generator
from tribualquestions.dbpedia.sparql_utils import getSparqlResults

BASE_URL = u'http://es.dbpedia.org/sparql'
CAT_QUERY = u"""SELECT * WHERE {{ ?uri dcterms:subject <http://{0}> .
                                  ?uri rdfs:label ?label .
                                  OPTIONAL {{?uri dbpedia-owl:thumbnail ?img .}}
                               }}
            """
NAME_QUERY = u"""SELECT * WHERE {{ ?uri dcterms:subject <http://{0}> .
                                  ?uri foaf:name ?label .
                                  OPTIONAL {{?uri dbpedia-owl:thumbnail ?img .}}
                               }}
            """

BIRTHPLACE_QUERY = u"""
PREFIX esdbp: <http://es.dbpedia.org/property/>
SELECT ?uri, ?label, ?url, MAX(?jaioterria) as ?jaioterria  WHERE {{ ?uri dcterms:subject <http://{0}> .
                  ?uri rdfs:label ?label
                  OPTIONAL {{?uri esdbp:lugarnac ?jaioterria .}}
                  OPTIONAL {{?uri esdbp:nacimiento ?jaioterria .}}
                  OPTIONAL {{?uri esdbp:origen ?jaioterria .}}
                  OPTIONAL {{?uri foaf:isPrimaryTopicOf ?url .}}
                               }}
GROUP BY ?uri ?label ?url
"""
BAD_IMAGES = re.compile(r'Escudo|COA|Falta|PP\.svg|Logo|Senyal|Coat|Madrid')
DATA_FILES_DIR = u'/devel/django/tribualquestions/data/'

"""
GROUPS[group] => (QTYPE, TITLE, QUERY, CHECKGENDER)
"""
GROUPS = {u'politicos': (u'¿Quien es?', u'¿Qué político aparece en la imagen?', CAT_QUERY, True),
         u'escritores': (u'¿Quien es?', u'¿Qué novelista aparece en la imagen?', CAT_QUERY, True),
         u'musicos': (u'¿Quien es?', u'¿Qué músico aparece en la imagen?', CAT_QUERY, True),
         u'tv': (u'¿Quien es?', u'Personaje de radio y televisión', CAT_QUERY, True),
         u'actores': (u'¿Quien es?', u'Actores y actrices', CAT_QUERY, True),
         u'animales': (u'Animales', u'¿De qué animal se trata?', NAME_QUERY, False)
         }
QUERY_MAP = (#(u'Políticos_de_España_de_ámbito_estatal', u'politicos'),
             (u'Líderes_autonómicos_del_Partido_Popular', u'politicos'),
             (u'Líderes_nacionales_del_Partido_Popular', u'politicos'),
             (u'Líderes_del_Partido_Popular', u'politicos'),
             (u'Líderes_nacionales_del_Partido_Socialista_Obrero_Español', u'politicos'),
             (u'Líderes_autonómicos_del_Partido_Socialista_Obrero_Español', u'politicos'),
             (u'Políticos_de_Podemos', u'politicos'),
             (u'Políticos_de_Convergència_i_Unió', u'politicos'),
             (u'Novelistas_de_España_del_siglo_XX',  u'escritores'),
             (u'Músicos_de_pop_de_España', u'musicos'),
             (u'Cantantes_de_pop_de_España', u'musicos'),
             (u'Presentadores_de_televisión_de_España', u'tv'),
             (u'Actores_de_cine_de_España', u'actores'),
             (u'Ganadoras_del_premio_TP_de_Oro_a_la_mejor_actriz', u'actores'),
             (u'Ganadores_del_premio_TP_de_Oro_al_mejor_actor', u'actores'),
             (u'Mamíferos_de_Europa', u'animales'),
             (u'Carnívoros_de_Europa', u'animales'),
             (u'Mamíferos_de_la_península_ibérica', u'animales'),
             (u'Aves_de_Europa', u'animales'),
             (u'Anfibios_de_Europa', u'animales'),
             (u'Reptiles_de_Europa', u'animales'),
             )

    
def _get_results(query, base_query):
    base = u'es.dbpedia.org/resource/Categoría:'
    full =  base + query
    results = getSparqlResults(BASE_URL, base_query.format(full))
    return results


def _load_names(path):
    data = []
    with open(path, 'r') as f:
        for line in f.readlines():
            a = line.split(',')
            data.append(a[0].lower())
    return data

def _find_jaioterria(result_dict):
    if result_dict.get('jaioterria') is not None:
        place = result_dict.get('jaioterria')
        try:
            p = int(place)
            return None
        except:
            pass
        if place == u'' or place.find('http')!= -1 or place.find('+') != -1:
            return None
        
        if place.find(',') != -1:
            place =  place[:place.find(',')]
        if len(place) > 0:
            return place.strip()


class DBPediaGenerator(Generator):
    """ """
    def clean_text(self, text):
        if text.find('(') != -1:
            return text[:text.find('(')]
        return text
    
    def check_quality(self, text):
        return True
    
    def _get_title(self, data_dict, **kwargs):
        return kwargs.get('title')

    def _get_correct_answer(self, data_dict, **kwargs):
        return data_dict.get('label')

            
    def _get_incorrect_answers(self, data_dict, **kwargs):
        wrong = kwargs.get('all_names')
        if len(wrong)<3:
            return (u'', u'')
        correct = data_dict.get('label')
        incorrect = []
        #bukaera gabeko begizta bat ekiditeko kontagailua
        kont = 0
        while len(incorrect) != 2 and kont < len(wrong):
            kont += 1
            index = random.randint(0, len(wrong)-1)
            current = wrong[index]
            if ( current not in incorrect) and (current != correct):
                cleaned = self.clean_text(current)
                if self.check_quality(cleaned):
                    incorrect.append(cleaned)
        return incorrect
    
    def _get_url(self, data_dict, **kwargs):
        return data_dict.get('uri')

    def _get_photo_url(self, data_dict, **kwargs):
        return data_dict.get('img')

    def _get_provider(self, data_dict, **kwargs):
        return u'Wikipedia'

    def _get_attribution(self, data_dict, **kwargs):
        return u'http://es.wikipedia.org'
    
class BirthPlaceGenerator(DBPediaGenerator):
    qtype = u'Lugar de nacimiento'
    title = u'¿Donde nació {nombre} ({categoria})?'

    def _get_title(self, data_dict, **kwargs):
        return  self.title.format(nombre=data_dict.get('label'), categoria=kwargs.get('title'))
    
    def _get_correct_answer(self, data_dict, **kwargs):
        return _find_jaioterria(data_dict)

    def _get_url(self, data_dict, **kwargs):
        return data_dict.get('url')
            
def _create_questions(correct_list, all_names, qtype, title, generator_type=DBPediaGenerator):
    for correct in correct_list:
        generator = generator_type()
        generator.qtype = qtype
        generator.create(obj=correct, **locals())
        generator.submit()
        generator.print_galdera()

def _create_gender_category_questions(results, qtype, title):
    females = _load_names(DATA_FILES_DIR + u'mujeres.csv')                        
    male = defaultdict(list)
    female = defaultdict(list)
    for result in results:
        name = result.get('label')
        check = name.split(' ')[0]
        if check.lower() in females:
            female['names'].append(name)
            if 'img' in result.keys():
                url = result.get('img')
                if not BAD_IMAGES.search(url):
                    female['with_images'].append(result)
        else:
            male['names'].append(name)
            if 'img' in result.keys():
                url = result.get('img')
                if not BAD_IMAGES.search(url):
                    male['with_images'].append(result)        
    _create_questions(female['with_images'], female['names'], qtype, title)
    _create_questions(male['with_images'], male['names'], qtype, title)
        
def _create_category_questions(results, qtype, title):
    names = []
    with_images = []
    for result in results:
        names.append(result.get('label'))
        if 'img' in result.keys():
            url = result.get('img')
            if not BAD_IMAGES.search(url):
                with_images.append(result)
    _create_questions(with_images, names, qtype, title)


def create_category_questions():
    for category, group in QUERY_MAP:
        qtype, title, base_query, check_gender = GROUPS.get(group, (None,None,None,None))
        if base_query == None:
            continue
        results = _get_results(category, base_query)
        if check_gender:
            _create_gender_category_questions(results, qtype, title)
        else:
            _create_category_questions(results, qtype, title)

def create_grouped_category_questions():
    groups = defaultdict(list)
    for category, group in QUERY_MAP:
        qtype, title, base_query, check_gender = GROUPS.get(group, (None,None,None,None))
        if base_query == None:
            continue
        groups[group].extend(_get_results(category, base_query))
    for category, group in QUERY_MAP:
        qtype, title, base_query, check_gender = GROUPS.get(group, (None,None,None,None))    
        if check_gender:
            _create_gender_category_questions(groups[group], qtype, title)
        else:
            _create_category_questions(groups[group], qtype, title)
            
def create_donde_nacio_questions():
    groups = defaultdict(list)
    excluded_groups = ('animales',)
    for category, group in QUERY_MAP:
        if group not in excluded_groups:
            groups[group].extend(_get_results(category, BIRTHPLACE_QUERY))
    
    for result_cat, results in groups.items():
        places = set()
        good = []
        for result in results:
            place = _find_jaioterria(result)
            if place is not None:
                places.add(place)
                good.append(result)        
        _create_questions(good, list(places), qtype='Lugar de nacimiento', title=result_cat, generator_type=BirthPlaceGenerator)
