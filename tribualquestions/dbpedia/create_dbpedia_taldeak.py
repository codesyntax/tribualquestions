import random
import requests
import urllib
import logging

from django.core.management.base import BaseCommand

from tribualserver.models import Question, QuestionType
from tribualserver.management.commands.sparql_utils import getSparqlResults
from tribualserver.utils.load_images import loadUrlImage

QTYPE_TITLE = 'Nor da?(bing)'
QTYPE_TITLE = 'Animaliak'
QTYPE_TITLE = 'Zer da?'
BASE_URL = 'http://eudbpedia.deusto.es/sparql'
BASE_QUERY = u"""SELECT ?uri ?label ?img WHERE { ?uri dcterms:subject category:%(taldea)s .
                           ?uri rdfs:label ?label .
                           OPTIONAL {?uri dbpedia-owl:thumbnail ?img .}
                           FILTER (lang(?label) = "eu")
                         } 
            """ 
BASE_QUERY = u"""SELECT ?uri ?label ?img WHERE { ?uri dcterms:subject category:%(taldea)s .
                           ?uri rdfs:label ?label .
                           ?uri dbpedia-owl:thumbnail ?img .
                           FILTER (lang(?label) = "eu")
                         } 
            """             
TALDEAK = {"Euskaltzainak": "Euskaltzaina",
           "Euskal_Kazetariak": "Euskal kazetaria",
           "Euskarazko_saiakeragileak": "Euskarazko saiakeragilea",
           "Euskal_idazleak": "Euskal idazlea",
           "Gipuzkoako_bertsolariak": "Gipuzkoako bertsolaria",
           "Bizkaiko_bertsolariak": "Bizkaiko bertsolaria",
           "Nafarroako_bertsolariak": "Nafarroako bertsolaria",
           "Arabako_bertsolariak": "Arabako bertsolaria",
           "Lapurdiko_bertsolariak": "Lapurdiko bertsolaria",
           "Euskal_Herriko_Soziologoak": "Euskal Herriko soziologoa",
           "Euskal_Herriko_dantzariak": "Euskal Herriko dantzaria",
           "Euskal_Herriko_aktoreak": "Euskal Herriko aktorea",
           "Gipuzkoako_politikariak": "Gipuzkoako politikaria",
           "Bizkaiko_politikariak": "Bizkaiko politikaria",
           "Arabako_politikariak": "Arabako politikaria",
           "Nafarroako_politikariak": "Nafarroako politikaria",
           "Lapurdiko_politikariak": "Lapurdiko politikaria",
           "Zuberoako_politikariak": "Zuberoako politikaria",
           "Nafarroako_kirolariak": "Nafarroako kirolaria",
           "Gipuzkoako_kirolariak": "Gipuzkoako kirolaria",
           "Bizkaiko_kirolariak": "Bizkaiko kirolaria",
           "Arabako_kirolariak": "Arabako kirolaria",
           "Lapurdiko_kirolariak": "Lapurdiko kirolaria",
           "Gipuzkoako_ahaldun_nagusiak": "Gipuzkoako ahaldun nagusia",
           "Arabako_ahaldun_nagusiak": "Arabako ahaldun nagusia",
           "Euskal_Herriko_futbolariak": "Euskal Herriko futbolaria",
           "Euskal_Herriko_txirrindulariak": "Euskal Herriko txirrindularia",
           "Euskal_Herriko_pilotariak": "Euskal Herriko pilotaria",
           "Euskararen_sustatzaileak": "Euskararen sustatzailea",
           "Euskal_Herriko_ingeniariak": "Euskal Herriko ingeniaria",
           "Euskaltzain_urgazleak": "Euskaltzain urgazlea",
           "Euskal_Herriko_Unibertsitateko_irakasleak": "Euskal Herriko Unibertsitateko irakaslea",
           "Euskal_Herriko_historialariak": "Euskal Herriko historialaria",
           "Euskal_Herriko_enpresaburuak": "Euskal Herriko enpresaburua",
           "Euskal_Herriko_itsaslariak": "Euskal Herriko itsaslaria",
           "Euskal_Herriko_telebista_aurkezleak": "Euskal Herriko telebista aurkezlea",
           "Euskal_Herriko_kazetariak": "Euskal Herriko kazetaria",
           "Euskal_Herriko_sindikalistak": "Euskal Herriko sindikalista",
           "Lapurdiko_musikagileak": "Lapurdiko musikagilea",
           "Arabako_musikagileak": "Arabako musikagilea",
           "Zuberoako_abeslariak": "Zuberoako abeslaria",
           "Euskal_Herriko_aktoreak": "Euskal Herriko aktorea",
           "Gipuzkoako_musikagileak": "Gipuzkoako musikagilea",
           "Blogariak": "Blogaria",
           "Bizkaiko_musikagileak": "Bizkaiko musikagilea",
           "Euskal_Herriko_irrati_esatariak": "Euskal Herriko irrati esataria",
           "Gasteizko_alkateak": "Gasteizko alkatea",
           "Donostiako_alkateak": "Donostiako alkatea",
           "Bilboko_alkateak": "Bilboko alkatea",
                      }

TALDEAK = {"Poligonoak": "Poligonoa",}
"""
"Euskal_Herriko_kontinentar_ugaztunak": "Ugaztuna"
"Intsektuak": "Intsektua",}
"""

TALDEAK = {"Bandera_nazionalak": "Banderak",
           "Armarri_nazionalak": "Armarriak",
           }
"""
"Gurutzeak": "Gurutzea",
"Ikurrak": "Ikurra",}
"""
TALDEAK = {"Bandera_nazionalak": "Banderak",
           "Armarri_nazionalak": "Armarriak",
           "Euskal_Herriko_kontinentar_ugaztunak": "Ugaztuna",
           "Poligonoak": "Poligonoa",}

class Command(BaseCommand):
    log = logging.getLogger('__name__')
    def _get_image_from_bing(self, search_string):
        minimun_width = 200
        key = "+V7Q2W4neiQCysV0va+vqXswX8ZR0zFw33Uf8yTdwl0="
        search = urllib.quote('\''+search_string + '\'')
        bing_search = u"https://api.datamarket.azure.com/Bing/Search/v1/Image?Query=" + search + u"&$format=json"
        response = requests.get(bing_search, auth=(key, key))
        try:
            jemaitzak = response.json()
            emaitzak = jemaitzak['d']['results']
        except:
            return None
        url = None
        """
        for em in emaitzak:
            if em.get('Width') >= minimun_width:
                url = em.get('MediaUrl')
        """
        if url is None: 
            url = emaitzak[0].get('MediaUrl')
        print 'Image from bing'
        return url
    
    def handle(self, *args, **options):
        qtype, created = QuestionType.objects.get_or_create(title=QTYPE_TITLE,active=False)

        for taldea, testua in TALDEAK.items():
            partaideak = set()
            emaitza = getSparqlResults(BASE_URL, BASE_QUERY % {'taldea': taldea})
            print 'Emaitza kopurua ' + str(len(emaitza))
            for em in emaitza:
                label = em.get('label')
                if label is not None:
                    partaideak.add(label.strip())
            for em in emaitza:
                title = testua
                correct_answer = em.get('label')
                if correct_answer is not None:
                    correct_answer = correct_answer.strip()
                else:
                    continue
                correct_set = set([correct_answer,])
                choose_incorrect = list(partaideak.difference(correct_set))
                random.shuffle(choose_incorrect)
                provider = 'Wikipedia'
                url = em.get('uri').replace('eu.dbpedia.org/resource','eu.wikipedia.org/wiki')
                image_url = None
                try:
                    image_url = em.get('img')
                    if image_url is None:
                        image_url = self._get_image_from_bing(correct_answer.encode('utf8')) 
                except Exception, e:
                    self.log.exception(e)
                print image_url
                photo = loadUrlImage(image_url, title=u'dbpedia ' + correct_answer)
                if not photo:
                    print 'Errorea argazki hau sortzean'
                    continue

                print title
                if len(choose_incorrect)>=2:
                    incorrect_one = choose_incorrect[0]
                    incorrect_two = choose_incorrect[1]
                    question = Question.objects.create(
                        title= title,
                        qtype=qtype,
                        desc=u'',
                        correct_answer=correct_answer,
                        incorrect_answer_one=incorrect_one,
                        incorrect_answer_two=incorrect_two,
                        provider=provider,
                        url=url,
                        photo_url=photo)
            print '--------------------------------------'

