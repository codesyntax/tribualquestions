from SPARQLWrapper import SPARQLWrapper

def getSparqlResults(base_url, query, raw=False):
    sparql = SPARQLWrapper(base_url)
    sparql.setQuery(query)
    results = sparql.query().convert()
    if raw:
        return results
    values = []
    for result in results.getElementsByTagName('result'):
        helper = {}
        bindings = result.getElementsByTagName('binding')
        for binding in bindings:
            valuechild=binding.firstChild.childNodes[0].nodeValue
            helper[binding.getAttribute('name')] = valuechild
        values.append(helper)
    return values
