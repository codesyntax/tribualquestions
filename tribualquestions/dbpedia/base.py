# -*- coding: utf-8 -*-
import re
import random
from collections import defaultdict

from tribualquestions.generator import Generator
from tribualquestions.dbpedia.sparql_utils import getSparqlResults

BAD_IMAGES = re.compile(r'Escudo|COA|Falta|PP\.svg|Logo|Senyal|Coat|Madrid')
QUERIES = (
    u"""
SELECT * WHERE {{ ?uri dcterms:subject <http://{0}> .
                                  ?uri rdfs:label ?label .
                                  OPTIONAL {{?uri dbpedia-owl:thumbnail ?img .}}
                                  FILTER (lang(?label) = "en")
                               }}
    """,
    u"""
SELECT * WHERE {{ ?uri dcterms:subject <http://{0}> .
                                  ?uri rdfs:label ?label .
                                  OPTIONAL {{?uri dbpedia-owl:thumbnail ?img .}}
                               }}
    """,
    )

QUERY_DATA = {'en': (u'http://dbpedia.org/sparql', u'dbpedia.org/resource/Category:',0),
              'es': (u'http://es.dbpedia.org/sparql', u'es.dbpedia.org/resource/Categoría:', 1),
              'eu': (u'http://eu.dbpedia.org/sparql', u'eu.dbpedia.org/resource/Kategoria:', 1),
              'pl': (u'http://pl.dbpedia.org/sparql', u'pl.dbpedia.org/resource/Kategoria:', 1),
              'de': (u'http://de.dbpedia.org/sparql', u'de.dbpedia.org/resource/Kategorie:', 1),
              }

class DBPediaGenerator(Generator):
    """ """
    def clean_text(self, text):
        if text.find('(') != -1:
            return text[:text.find('(')]
        return text
    
    def check_quality(self, text):
        return len(text.split(' ')) < 4
            
    def _get_title(self, data_dict, **kwargs):
        return kwargs.get('title')

    def _get_correct_answer(self, data_dict, **kwargs):
        correct = self.clean_text(data_dict.get('label'))
        if not self.check_quality(correct):
            correct = u''
        return correct
            
    def _get_incorrect_answers(self, data_dict, **kwargs):
        wrong = kwargs.get('all_names')
        if len(wrong)<3:
            return (u'', u'')
        correct = data_dict.get('label')
        incorrect = []
        kont = 0
        while len(incorrect) != 2 and kont < len(wrong):
            kont += 1
            index = random.randint(0, len(wrong)-1)
            current = wrong[index]
            if (wrong[index] not in incorrect) and (wrong[index] != correct):
                cleaned = self.clean_text(current)
                if self.check_quality(cleaned):
                    incorrect.append(cleaned)
        if len(incorrect) != 2:
            print incorrect
            return ['','']
        else:
            return incorrect
    
    def _get_url(self, data_dict, **kwargs):
        return unicode(data_dict.get('uri'))

    def _get_photo_url(self, data_dict, **kwargs):
        return data_dict.get('img')

    def _get_provider(self, data_dict, **kwargs):
        return u'Wikipedia'
    """
    def _get_attribution(self, data_dict, **kwargs):
        return u'http://es.wikipedia.org'
    """


def get_question(correct, all_names, qtype, title):
    """
    correct_list: url,label eta img eremuak dituen hiztegia. Galderen erantzun zuzenak sortzeko
    all_names: erantzun okerrak sortzeko erabiliko diren izen zerrenda
    """
    generator = DBPediaGenerator()
    generator.qtype = qtype
    generator.create(obj=correct, **locals())
    return generator

def get_names_from_queryset(results):
    """
    sparql query baten emaitza jaso eta bi lista bueltatu. Bata irudidun
    sarrerekin eta bestea queriset-eko izen guztiekin
    """
    names = []
    with_images = []
    for result in results:
        names.append(result.get('label'))
        if 'img' in result.keys():
            url = result.get('img')
            if not BAD_IMAGES.search(url):
                with_images.append(result)
    return with_images, names


def get_results(query, base_url, base_path, base_query):
    full =  base_path + query
    try:
        results = getSparqlResults(base_url, base_query.format(full))
    except:
        import pdb;pdb.set_trace()
    return results


def same_category(cat_data, qtype, title, attribution='http://wikipedia.org', debug=False):
    queryset = get_results(cat_data.get('category'),
                           cat_data.get('source')[0],
                           cat_data.get('source')[1],
                           QUERIES[cat_data.get('source')[2]])
    correct, available = get_names_from_queryset(queryset)
    for c in correct:
        question  = get_question(c, available, qtype, title)
        question.attribbution = attribution
        if not debug:
            question.submit()
        question.print_galdera()

def same_group(cat_list, qtype, title, attribution='http://wikipedia.org', debug=False):
    correct = defaultdict(list)
    available = defaultdict(list)
    for cat_data in cat_list:
        temp_c = []
        temp_a = []
        queryset = get_results(cat_data.get('category'),
                               cat_data.get('source')[0],
                               cat_data.get('source')[1],
                               QUERIES[cat_data.get('source')[2]])
        temp_c, temp_a = get_names_from_queryset(queryset)
        genre = cat_data.get('genre')
        if genre is not None:
            correct[genre].extend(temp_c)
            available[genre].extend(temp_a)
        else:
            correct['nogenre'].extend(temp_c)
            available['nogenre'].extend(temp_a)
    for k in correct.keys():
        for c in correct.get(k):
            available_list = available.get(k)
            question  = get_question(c, available_list, qtype, title)
            question.attribbution = attribution
            if not debug:
                question.submit()
            question.print_galdera()

def all_mixed(group_list, qtype, title, attribution='http://wikipedia.org', debug=False):
    correct = defaultdict(list)
    available = defaultdict(list)
    for group in group_list:
        for cat_data in group:
            temp_c = []
            temp_a = []
            queryset = get_results(cat_data.get('category'),
                                   cat_data.get('source')[0],
                                   cat_data.get('source')[1],
                                   QUERIES[cat_data.get('source')[2]])
            temp_c, temp_a = get_names_from_queryset(queryset)
            genre = cat_data.get('genre')
            if genre is not None:
                correct[genre].extend(temp_c)
                available[genre].extend(temp_a)
            else:
                correct['nogenre'].extend(temp_c)
                available['nogenre'].extend(temp_a)                
    for k in correct.keys():
        for c in correct.get(k):
            available_list = available.get(k)
            question  = get_question(c, available_list, qtype, title)
            question.attribbution = attribution
            if not debug:
                question.submit()
            question.print_galdera()
