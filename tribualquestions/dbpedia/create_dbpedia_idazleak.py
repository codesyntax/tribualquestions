import random

from django.core.management.base import BaseCommand

from tribualserver.models import Question, QuestionType

from tribualserver.management.commands.sparql_utils import getSparqlResults

BASE_URL = 'http://eudbpedia.deusto.es/sparql'
araba_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Araba .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
bizkaia_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Bizkaia .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """

gipuzkoa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Gipuzkoa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """

lapurdi_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Lapurdi .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
zuberoa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Zuberoa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
nafarroa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Nafarroa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
nafarroa_beherea_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Nafarroa_Beherea .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
query_dict = {
    'araba': araba_query,
    'bizkaia': bizkaia_query,
    'gipuzkoa': gipuzkoa_query,
    'lapurdi': lapurdi_query,
    'nafarroa': nafarroa_query,
    'nafarroa_behera': nafarroa_beherea_query,
    'zuberoa': zuberoa_query
    }
TOEXCLUDE = ('Euskadi', 'Euskal Herria', 'Bizkaia', 'Gipuzkoa', 'Nafarroa', 'Kantabria', 'Enkarterri', 'Lapurdi', 'Katalunia', 'Leongo probintzia', '?', 'Araba', 'Nafarroa Beherea', 'Zuberoa','Tolosaldea','Olaso dorrea')
class Command(BaseCommand):
        
    def handle(self, *args, **options):
        qtype, created = QuestionType.objects.get_or_create(title='Non jaio zen?')
        galdera = '. Non jaio zen?'
        for k,v in query_dict.items():
            herriak = set()
            emaitza = getSparqlResults(BASE_URL, v)
            print 'Emaitza kopurua ' + str(len(emaitza))
            for em in emaitza:
                if em.get('birthPlace') not in TOEXCLUDE:
                    herriak.add(em.get('birthPlace'))
            for em in emaitza:
                if em.get('birthPlace') in TOEXCLUDE:
                    continue
                title = em.get('abstract').split('.')
                if len(title):
                    title = title[0]
                else:
                    title = em.get('name')
                if len(title + galdera) > 255:
                    title = title[:255-len(galdera)]
                correct_answer = em.get('birthPlace')
                correct_set = set([correct_answer])
                choose_incorrect = list(herriak.union(correct_set))
                random.shuffle(choose_incorrect)
                provider = 'Wikipedia'
                url = em.get('uri')
                print title
                if len(choose_incorrect)>=2:
                    incorrect_one = choose_incorrect[0]
                    incorrect_two = choose_incorrect[1]
                    question = Question.objects.create(
                        title= title + galdera,
                        qtype=qtype,
                        desc=u'',
                        correct_answer=correct_answer,
                        incorrect_answer_one=incorrect_one,
                        incorrect_answer_two=incorrect_two,
                        provider=provider,
                        url=url)
            print '--------------------------------------'

