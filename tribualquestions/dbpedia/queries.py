# -*- coding: utf-8 -*-
from SPARQLWrapper import SPARQLWrapper

araba_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Araba .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  OPTIONAL {?uri dbpedia-owl:abstract ?abstract .}
                  }
        """
bizkaia_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Bizkaia .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """

gipuzkoa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Gipuzkoa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """

lapurdi_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Lapurdi .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
zuberoa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Zuberoa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
nafarroa_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Nafarroa .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
nafarroa_beherea_query = u"""SELECT DISTINCT ?name, ?uri, ?birthPlace, ?abstract
           WHERE {?uri ?x dbpedia:Nafarroa_Beherea .
                  ?uri dbpedia-owl:birthPlace ?birthPlaceUrl .
                  ?uri foaf:name ?name .
                  ?uri dbpedia-owl:abstract ?abstract .
                  ?birthPlaceUrl rdfs:label ?birthPlace .
                  }
        """
query_dict = {
    'araba': araba_query,
    'bizkaia': bizkaia_query,
    'gipuzkoa': gipuzkoa_query,
    'lapurdi': lapurdi_query,
    'nafarroa': nafarroa_query,
    'nafarroa_behera': nafarroa_beherea_query,
    'zuberoa': zuberoa_query
    }
    
def getSparqlResults(base_url, query):
    sparql = SPARQLWrapper(base_url)
    sparql.setQuery(query)
    results = sparql.query().convert()
    values = []
    for result in results.getElementsByTagName('result'):
        helper = {}
        bindings = result.getElementsByTagName('binding')
        for binding in bindings:
            valuechild=binding.firstChild.childNodes[0].nodeValue
            helper[binding.getAttribute('name')] = valuechild
        values.append(helper)
    return values

if __name__ == '__main__':
    emaitza = {}
    BASE_URL = 'http://eudbpedia.deusto.es/sparql'
    for probintzia, query in query_dict.items():
        emaitza[probintzia] = getSparqlResults(BASE_URL, query)
    
    for probintzia,result in emaitza.items():
        print probintzia
        print '----------------------------------'
        for r in result:
            print r
        print '----------------------------------'
        print len(result)
        print '----------------------------------'


