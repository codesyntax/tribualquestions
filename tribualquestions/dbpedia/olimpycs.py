from tribualquestions.dbpedia.base import QUERY_DATA, same_group, all_mixed

QUERIES = (
    u"""
SELECT * WHERE {{ ?uri dcterms:subject <http://{0}> .
                                  ?uri rdfs:label ?label .
                                  OPTIONAL {{?uri dbpedia-owl:thumbnail ?img .}}
                                  FILTER (lang(?label) = "en")
                               }}
    """,
)
    
olimpics = [
    {'source': QUERY_DATA['en'],
     'category': u'Summer_Olympic_Games',
     'genre': None
     },
    {'source': QUERY_DATA['en'],
     'category': u'Winter_Olympic_Games',
     'genre': None
     },
    ]

def create_all_grouped():
    data = (
        (olimpics, 'olimpics'),
        )
    for d in data:
        same_group(d[0], qtype=u'
            
        
