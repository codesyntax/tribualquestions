import string
import Levenshtein
from .hiztegibatua import HiztegiBatua
from .hitza_asmatu import AsmatuHitzaGenerator
        
def eman_hitzak(xml_path, luzera_min=None, luzera_max=None, qtype=None ):
    ag = AsmatuHitzaGenerator()
    if luzera_min is not None:
        ag.luzera_min = luzera_min
    if luzera_max is not None:
        ag.luzera_max = luzera_max
    ag.get_xml_tree(xml_path)
    entries = ag.find_nongeo_entries()
    
    i = 1
    izenak = []
    for entry in entries:
        mota = entry.find(".//gramGrp/pos/q")
        if mota is not None:
            mota = mota.text
        else:
            mota = 'iz.'
        if mota=='iz.':
            zuzena = entry.find(".//orth")
            if zuzena is not None:
                text = zuzena.text
                if len(text) > luzera_min and len(text)<=luzera_max and text.find(' ')==-1:
                    izenak.append(text.upper())
                    i += 1
    return izenak

def letra_berdinak(a,b):
    a1 = list(a)
    b1 = list(b)
    a1.sort()
    b1.sort()
    return a1==b1


def antzekoak_bilatu(hitza, hitzak):
    MIN_R = 0.8
    ordenatuak  = []
    for hitza2 in hitzak:
        r = Levenshtein.ratio(unicode(hitza),unicode(hitza2))
        if r < 1 and r > MIN_R: #bestela iguala da edo ez du antz haundirik
            #letra igualak --beste ordena batean-- baditu ere, ez gehitu
            if not letra_berdinak(unicode(hitza),unicode(hitza2)):
                ordenatuak.append((hitza2,r))

    ordenatuak = sorted(ordenatuak,key=lambda x: x[1], reverse=True)
    return ordenatuak[:2]