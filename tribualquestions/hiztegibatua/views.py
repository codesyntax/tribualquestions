from tribualquestions.generator import Generator
import random
from .hitza_ordenatu import antzekoak_bilatu

class OrdenatuEtaOsatuHitzGenerator(Generator):
    qtype = u'Ordenatu eta osatu hitza'
    provider = u'Euskaltzaindia - Hiztegi Batua'

    def _get_title(self, obj,**kwargs):
        return "".join(random.sample(obj, len(obj)))

    def _get_desc(self, obj,**kwargs):
        return u''

    def _get_photo_url(self, obj,**kwargs):
        return None

    def _get_correct_answer(self, obj,**kwargs):
        return obj
    
    def _get_incorrect_answers(self, obj,**kwargs): 
        okerrak = antzekoak_bilatu(obj,kwargs['hitzak'])
        if len(okerrak) > 1:
            return (okerrak[0][0], okerrak[1][0])
        else:
            return ('','')

    def _get_url(self, obj,**kwargs):
        return u''

    def _get_attribution(self, obj,**kwargs):
        return u'Euskaltzaindia - Hiztegi Batua'