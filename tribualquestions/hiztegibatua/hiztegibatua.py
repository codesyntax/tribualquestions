from collections import defaultdict

import xml.etree.ElementTree as ET

from tribualquestions.generator import Generator

class HiztegiBatua(Generator):
    BALIOKIDETZAK = {'Naf.': 'Nafarroan',
                     'Bizk.': 'Bizkaian',
                     'Heg.': 'Hegoaldean',
                     'Ipar.': 'Iparraldean',
                     'Ipar': 'Iparraldean',
                     'Gip.': 'Gipuzkoan',
                     'Gip': 'Gipuzkoan',
                     'Zub.': 'Zuberoan',
                     'Lap.': 'Lapurdin',
                     'Beh.': 'Behe Nafarroan',
                     'BNaf.': 'Behe Nafarroan',
                     'Amik.': 'Amikuze',
                     'Am.': 'Amikuze',
                    }

    SARRERAK = defaultdict(list)
    SARRERAK = {}
    BASE_URL = 'www.euskaltzaindia.net/index.php?sarrera=%(search)s&option=com_hiztegianbilatu&view=frontpage&Itemid=410&lang=eu'
    
    provider = u'Euskaltzaindia'
    attribution = u'Euskaltzaindia'
    
    tree = None
    
    def get_xml_tree(self, xml_path):
        f = open(xml_path)
        self.tree = ET.parse(f)


    def find_geo_entries(self):
        result = []
        entries = self.tree.findall("entry")
        for entry in entries:
            self.update_sarrerak(entry)
            if entry.findall(".//*[@type='geo']"):
                result.append(entry)
        return result
    
    def find_nongeo_entries(self):
        result = []
        entries = self.tree.findall("entry")
        for entry in entries:
            if not entry.findall(".//*[@type='geo']"):
                result.append(entry)
        return result
    
    def update_sarrerak(self, element):
        kategoria = element.find(".//gramGrp/pos/q")
        if kategoria is not None:
            orth = element.find('.//orth')
            geo = element.findall(".//*[@type='geo']/q")
            if geo is not None:
                for g in geo:
                    baliokidetzan = self.BALIOKIDETZAK[g.text]
                    if baliokidetzan not in self.SARRERAK.keys():
                        self.SARRERAK[baliokidetzan]=defaultdict(list)
                    if orth is not None:
                        self.SARRERAK[baliokidetzan][kategoria.text].append(orth.text)
        return None
