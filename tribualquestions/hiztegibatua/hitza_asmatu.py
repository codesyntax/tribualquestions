import random
import string

from .hiztegibatua import HiztegiBatua
        
class AsmatuHitzaGenerator(HiztegiBatua):
    
    qtype = ''
    luzera_min = 10
    luzera_max = 100
    
    ###
    # Generator interface
    ###
    def _get_title(self, datuak, **kwargs):
        return datuak.get('galdera')

    def _get_correct_answer(self, datuak, **kwargs):
        return datuak.get('zuzena')

    def _get_incorrect_answers(self, datuak, **kwargs):
        return (datuak.get('okerra1'), datuak.get('okerra2'))

    def _ger_url(self, datuak, **kwargs):
        return datuak.get('url')
    ###
    # End interface
    ###

    def get_okerrak(self,zerrenda,luzera,posizioa,letra):
        random.shuffle(zerrenda)
        i = random.randint(1,3)
        okerrak = []
        if i==1:
            #denek luzera igoala, letra aurrekoan edo ondorengoan
            for izena in zerrenda:
                if len(izena) == luzera and izena[posizioa] != letra and izena[posizioa-1] == letra:
                    okerrak.append(izena)
                    if len(okerrak) > 1:
                        return (okerrak[0], okerrak[1])
                if len(izena) == luzera and izena[posizioa] != letra and izena[posizioa+1] == letra:
                    okerrak.append(izena)
                    if len(okerrak) > 1:
                        return (okerrak[0], okerrak[1])
        elif i==2:
            #letra ondo baina motzagoa edo luzeagoa pixkat
            for izena in zerrenda:
                if len(izena) == luzera +1 and izena[posizioa] == letra:
                    okerrak.append(izena)
                    if len(okerrak) > 1:
                        return (okerrak[0], okerrak[1])
                if len(izena) == luzera -1 and izena[posizioa] == letra:
                    okerrak.append(izena)
                    if len(okerrak) > 1:
                        return (okerrak[0], okerrak[1])
        else:
            #bat motzagoa edo luzeagoa baina letra gaizki
            #besteak luzera igoala, letra gaizki
            okerrak1 = ''
            okerrak2 = ''
            for izena in zerrenda:
                if len(izena) == luzera +1 and izena[posizioa] == letra:
                    okerrak1 = izena
                if len(izena) == luzera -1 and izena[posizioa] == letra:
                    okerrak1 = izena
                if len(izena) == luzera and izena[posizioa] != letra and izena[posizioa-1] == letra:
                    okerrak2 = izena
                if len(izena) == luzera and izena[posizioa] != letra and izena[posizioa+1] == letra:
                    okerrak2 =  izena
                if okerrak1 and okerrak2:
                    return (okerrak1, okerrak2)

        return None
    

def sortu_galderak(xml_path, luzera_min=None, luzera_max=None, qtype=None ):
    ag = AsmatuHitzaGenerator()
    if luzera_min is not None:
        ag.luzera_min = luzera_min
    if luzera_max is not None:
        ag.luzera_max = luzera_max
    ag.get_xml_tree(xml_path)
    entries = ag.find_nongeo_entries()
    
    i = 1
    izenak = []
    for entry in entries:
        mota = entry.find(".//gramGrp/pos/q")
        if mota is not None:
            mota = mota.text
        else:
            mota = 'iz.'
        if mota=='iz.':
            zuzena = entry.find(".//orth")
            if zuzena is not None:
                text = zuzena.text
                if len(text) > luzera_min and len(text)<=luzera_max and text.find(' ')==-1:
                    izenak.append(text.upper())
                    i += 1
    for izena in izenak:
        datuak = {} 
        izena = izena
        pos = int(len(izena)/3)
        i = random.randint(pos,len(izena)-pos)
        galdera = u' _ ' * (i)
        galdera += izena[i]
        galdera += u' _ ' * (len(izena) - i -1 )
        galdera = galdera.strip()
        luzera = len(izena)
        letra = izena[i]
        okerrak = ag.get_okerrak(izenak,luzera,i,izena[i])

        if okerrak and letra in string.ascii_letters:
            datuak['zuzena'] = izena
            datuak['galdera'] = galdera
            datuak['okerra1'] = okerrak[0]
            datuak['okerra2'] = okerrak[1]
            datuak['url'] = ag.BASE_URL % {'search': unicode(izena)}
            try:
                a = 1
            except:
                print 'Errorea unicode galdera sortzerakoan', izena

    
            ag.create(datuak, qtype=qtype)
            ag.submit()
            ag.print_galdera()
