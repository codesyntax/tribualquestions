# README #

Iturri desberdinetatik galderak sortzeko moduluak inplementatzen dira tribualquesions-en

### Zertarako da? ###

* Eduki askeak eskaintzen dituzten iturri desberdinetatik galderak sortzeko erabili daiteke modulu hau
* Galdera sorkuntzarako beharrezko azpiegitura eskaintzen du, baita ere galdera hauek erabiliko dituen aplikaziora API dei bidez bidaltzeko aukera

### Instalazioa ###

pip install tribualquestion

### Konfigurazioa ###

* Django aplikazioan INSTALLED_APPS settings-ean gehitu tribualquestions
* Datubasea sinkronizatu
* API bidezko komunikazioa erabili nahi baldin bada, ezarri API_BASE_URL eta TRIBUAL_TOKEN aldagaiak (Token bidezko autentikazioa erabiltzen da api eskaerak egiteko)
* Galdera mota bakoitza sortzeko prozesua desberdina da, begiratu galdera mota bakoitza sortzeko zer egin behar den inplementazio bakoitzean.