from setuptools import setup, find_packages
import sys, os

version = '1.0'

setup(name='tribualquestions',
      version=version,
      description="",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='CodeSyntax',
      author_email='info@codesyntax.com',
      url='http://www.codesyntax.com',
      license='GPL3.0',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
          'BeautifulSoup4',
          'tweepy',
          'langid',
          'pygeocoder',
          'pyparsing',
          'requests',
          'python-Levenshtein',
          'sparqlwrapper',
          'unicodecsv',
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
